public class CustomerWorkOrderYearCalenderController{
    //Integer variables
    public integer month {get;set;}
    public integer year {get;set;}
    
    //String variables
    public String customerId;
    public string monthname {get;set;}
    public String workOrderDates;
    public String cancelWorkOrderDates;
    public String otherWorkOrderDates;
    public String eventWorkOrderDates;
    public String roomWorkOrderDates;
    public String serviceWorkOrderDates;
    
    //object variables
    public Account customer{get;set;}
    
    //Lists
    public List<list<integer>> days {get;set;}
    public List<WeekDaysModel> weekModels{get;set;}
    public List<Service_Work_Order__c> workOrders;
    
    //Sets
    public Set<String> WOServiceDates{get;set;}
    public Set<Date> allEventsDates;
    
    //Maps
    public Map<String,List<Work_Order_Room__c>> WoRoomsMap{get;set;}
    public Map<String,List<Work_Order_Service__c>> WoServicesMap{get;set;}
    
    //Constructor
    public CustomerWorkOrderYearCalenderController(){
        customerId = ApexPages.currentPage().getParameters().get('id');
        customer = new Account();
        List<Account> acc; 
        if(CustomerId!=null && CustomerId!=''){
            acc = [SELECT Id, Name, Street_Address_2__c, Sales_Zone__c,Location__r.Phone__c, Location__r.Branch_Name__c, Location__c, 
                             BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity ,Location__r.Name
                             FROM Account WHERE Id =: customerId];
        }
        System.Debug('Acc===>'+acc);
        if(acc != null && acc.size() > 0){
            customer = acc.get(0);
        }
    }
    
    //method for creating Work Order Rooms Map for Every Work Order functionality
    private void FindWorkOrderRooms(List<Service_Work_Order__c> worderList){
        
        set<string> woIdSet = new set<string>(); 
        
        for(Service_Work_Order__c rec: worderList){
            woIdSet.add(rec.id);
        }
        
        List<Work_Order_Room__c> WORooms = [SELECT Id, Name, Work_Order__c 
                                            FROM Work_Order_Room__c 
                                            WHERE Work_Order__c in : woIdSet];
        
        
        if(WORooms.size()>0){
            for(Work_Order_Room__c woroom : WORooms){
                if(WoRoomsMap.containsKey(woroom.Work_Order__c)){
                    List<Work_Order_Room__c> tempRoomList = WoRoomsMap.get(woroom.Work_Order__c);
                    tempRoomList.add(woroom);
                    WoRoomsMap.put(woroom.Work_Order__c, tempRoomList);
                }
                else{
                    List<Work_Order_Room__c> tempRoomList2 = new List<Work_Order_Room__c>();
                    tempRoomList2.add(woroom);
                    WoRoomsMap.put(woroom.Work_Order__c, tempRoomList2);
                }           
            }
        }
    }
    
    //method for creating Work Order Rooms Map for Every Work Order functionality
    private void FindWorkOrderServices(List<Service_Work_Order__c> worderList){
        
        SET<String> woIdSet = new SET<string>(); 
        
        for(Service_Work_Order__c rec: worderList){
            woIdSet.add(rec.id);
        }
        
        List<Work_Order_Service__c> WOServices = [SELECT Id, Name, Work_Order__c 
                                                  FROM Work_Order_Service__c
                                                  WHERE Work_Order__c in : woIdSet];
        
        if(WOServices.size()>0){
            for(Work_Order_Service__c woservice : WOServices){
                if(WoServicesMap.containsKey(woservice.Work_Order__c)){
                    List<Work_Order_Service__c> tempServiceList = WoServicesMap.get(woservice.Work_Order__c);
                    tempServiceList.add(woservice);
                    WoServicesMap.put(woservice.Work_Order__c, tempServiceList);
                }
                else{
                    List<Work_Order_Service__c> tempServiceList2 = new List<Work_Order_Service__c>();
                    tempServiceList2.add(woservice);
                    WoServicesMap.put(woservice.Work_Order__c, tempServiceList2);
                }           
            }
        }
    }
    
    //Method for finding all Event Dates for Customer Location Functionality
    private void FindEventDates(String custId){
        List<Event> events = new List<Event>();
        allEventsDates = new Set<Date>();
        String locationId = [SELECT Id, Location__c FROM Account WHERE Id =: custId].Location__c;
        
        if(locationId != null){
            events = [SELECT Id, Subject, IsAllDayEvent, EndDateTime, StartDateTime, WhatId 
                      FROM Event
                      WHERE WhatId =: locationId
                      AND Subject = 'holiday'];
            if(events.size() > 0){
                for(Event evnt : events){
                    Date startdate;
                    Date enddate;
                    if(evnt.IsAllDayEvent){
                        startdate = evnt.StartDateTime.date().addDays(1);
                        enddate = evnt.EndDateTime.date().addDays(1);
                    }else{
                        startdate = evnt.StartDateTime.date();
                        enddate = evnt.EndDateTime.date();
                    }
                    Date tempdate = startdate;
                    allEventsDates.add(startdate);
                    allEventsDates.add(enddate);
                    Integer numberOfDays = startDate.daysBetween(enddate);
                    Integer i;
                    for(i=1 ; i <= numberOfDays; i++){
                        System.debug('i ====>>>>'+ i);
                        if(tempdate < enddate){
                            System.debug('tempdate ====>>>>'+ tempdate);
                            allEventsDates.add(tempdate);  
                        }
                        tempdate = tempdate.addDays(1);  
                        System.debug('tempdate ====>>>>'+ tempdate);
                    }
                }
            }
        }
    }
    
    //Method create fullyear customer workorder calender 
    public void getPrintCalendar(){
      String isPdf = Apexpages.currentPage().getParameters().get('renderAs');
      
        workOrders     = new List<Service_Work_Order__c>();
        WOServiceDates = new Set<String>();
        WoRoomsMap     = new Map<String,List<Work_Order_Room__c>>();
        WoServicesMap  = new Map<String,List<Work_Order_Service__c>>();
        
        workOrders = [SELECT Work_Team__r.Deleted__c, Work_Team__r.Captain__r.Name, Work_Team__r.Captain__c,Status__c,Cancellation_Reason__c,
                      Work_Team__c, Service_Date__c, Service_Agreement__r.Customer__c, Service_Agreement__c, Id, Rescheduled_Reason__c 
                      FROM Service_Work_Order__c 
                      WHERE Service_Agreement__r.Customer__c =: customerId
                      AND CALENDAR_MONTH(Service_Date__c) =: month
                      AND CALENDAR_YEAR(Service_Date__c) =: year
                      AND Status__c = 'Scheduled'
                      ORDER BY Service_Date__c ASC];
                      
        FindEventDates(customerId);
        FindWorkOrderRooms(workOrders);
        FindWorkOrderServices(workOrders);
        
        workOrderDates        = '';   
        otherWorkOrderDates   = '';
        eventWorkOrderDates   = '';
        roomWorkOrderDates    = '';
        serviceWorkOrderDates = '';    
        
        //loop through Service Work Orders   
        for(Service_Work_Order__c swo : workOrders){
            if(swo.Status__c == 'Scheduled' && allEventsDates.contains(swo.Service_Date__c)){
                eventWorkOrderDates += swo.Service_Date__c.day()+',';
            }
            else if(swo.Status__c == 'Scheduled' && WoRoomsMap.containsKey(swo.id)){
                roomWorkOrderDates += swo.Service_Date__c.day()+',';
            }
            else if(swo.Status__c == 'Scheduled' && WoServicesMap.containsKey(swo.id)){
                serviceWorkOrderDates += swo.Service_Date__c.day()+',';
            }
            else{
                workOrderDates += swo.Service_Date__c.day()+',';
            }
        }
        
        System.Debug('workOrders===>>>'+ workOrders);
        
        days = new list<list<integer>>();
        days.add(new list<integer>{0,0,0,0,0,0,0});
        days.add(new list<integer>{0,0,0,0,0,0,0});
        days.add(new list<integer>{0,0,0,0,0,0,0});
        days.add(new list<integer>{0,0,0,0,0,0,0});
        days.add(new list<integer>{0,0,0,0,0,0,0});
        days.add(new list<integer>{0,0,0,0,0,0,0});
        days.add(new list<integer>{0,0,0,0,0,0,0});
        
        System.Debug('Days===>0+days');
        Date theDate;
        DateTime dtime;
        System.Debug('year'+year);
        System.Debug('month'+month);
        //if(year !=null && Month !=null){
            theDate      = Date.newInstance(year, month, 1);
            dtime    = DateTime.newInstance(year, month, 01, 0, 0, 0);
        
            monthname         = dtime.format('MMMM');
            Date weekStart    = theDate.toStartofWeek();
            Integer dayOfWeek = weekStart.daysBetween(theDate)+1;
            integer row = 0;
            
            if(dayOfWeek == 0){
                dayOfWeek = 6;
            }else{
                dayOfWeek--;
            }
        
            set<integer> hds = new set<integer>();
            set<integer> otherWodays   = new set<integer>();
            set<integer> eventWodays   = new set<integer>();
            set<integer> roomWodays    = new set<integer>();
            set<integer> serviceWodays = new set<integer>();
            
            //Filling Days Sets accoring Specific conditions
            hds = getDaysSet(workOrderDates);
            otherWodays      = getDaysSet(otherWorkOrderDates   );
            eventWodays      = getDaysSet(eventWorkOrderDates   );
            roomWodays       = getDaysSet(roomWorkOrderDates    );
            serviceWodays    = getDaysSet(serviceWorkOrderDates );
                   
            integer dayOfMonth = 1;
            
            Integer currentMon = Date.today().month();
            Integer yr = Date.today().year();
            Integer dte      = Date.today().toStartOfWeek().day();
            Integer dte1      = Date.today().toStartOfWeek().addDays(6).day();
            Integer dayCounter = 0;
            system.debug('dte1====>>>>'+dte1);
            //filling dates
            for(integer i=1;i<=Date.daysInMonth(year,month);i++){
                if(month == currentMon && year == yr && dte1 >6){
                  system.debug('====>>>>hi');
                  if(i >= dte){
                    if(days[row][dayCounter] < 0)
                        days[row][dayCounter] = -1 * i;
                    else
                        days[row][dayCounter] = i;
                    dayCounter++;
                    if(dayCounter > 6){
                        row++;
                        dayCounter = 0;
                    }
                  }
                }else{
                  if(month == currentMon && year > yr){
                    if(i < dte1){
                      if(days[row][dayOfWeek] < 0)
                          days[row][dayOfWeek] = -1 * i;
                      else
                          days[row][dayOfWeek] = i;
                      dayOfWeek++;
                      if(dayOfWeek > 6){
                          row++;
                          dayOfWeek = 0;
                      }
                    }
                  }else{
                    if(days[row][dayOfWeek] < 0)
                        days[row][dayOfWeek] = -1 * i;
                    else
                        days[row][dayOfWeek] = i;
                    dayOfWeek++;
                    if(dayOfWeek > 6){
                        row++;
                        dayOfWeek = 0;
                    }
                  }
                }
            }
        
        weekModels = new List<WeekDaysModel>();
        
        //setting up effects
        for(integer r=0;r<7;r++){
            WeekDaysModel weekModel = new WeekDaysModel();
            List<DaysModel> dayModels = new List<DaysModel>();
            
            for(integer c=0;c<7;c++){
                DaysModel dModel = new DaysModel();
                dModel.day = days[r][c];
                dModel.styleClas = 'day';
                if(days[r][c] == 0){
                    dModel.day = days[r][c];
                    dModel.styleClas = 'hidden';
                }
                if(isPdf != null && isPdf == 'pdf'){
                  if(hds.size() > 0 && hds.contains(days[r][c]) && days[r][c] > 0){
                      dModel.day = days[r][c];
                      dModel.styleClas = 'pdf';
                  }
                  if(eventWodays.size() > 0 && eventWodays.contains(days[r][c]) && days[r][c] > 0){
                      dModel.day = days[r][c];
                      dModel.styleClas = 'pdf';
                  }
                  if(roomWodays.size() > 0 && roomWodays.contains(days[r][c]) && days[r][c] > 0){
                      dModel.day = days[r][c];
                      dModel.styleClas = 'pdf';
                  }
                  if(serviceWodays.size() > 0 && serviceWodays.contains(days[r][c]) && days[r][c] > 0){
                      dModel.day = days[r][c];
                      dModel.styleClas = 'pdf';
                  }
                }else{
                  if(hds.size() > 0 && hds.contains(days[r][c]) && days[r][c] > 0){
                      dModel.day = days[r][c];
                      dModel.styleClas = 'other';
                  }
                  if(eventWodays.size() > 0 && eventWodays.contains(days[r][c]) && days[r][c] > 0){
                      dModel.day = days[r][c];
                      dModel.styleClas = 'event';
                  }
                  if(roomWodays.size() > 0 && roomWodays.contains(days[r][c]) && days[r][c] > 0){
                      dModel.day = days[r][c];
                      dModel.styleClas = 'room';
                  }
                  if(serviceWodays.size() > 0 && serviceWodays.contains(days[r][c]) && days[r][c] > 0){
                      dModel.day = days[r][c];
                      dModel.styleClas = 'service';
                  }
                }
                dayModels.add(dModel);
            }
            
            weekModel.dayModels = dayModels;
            weekModels.add(weekModel);
        }
      
    }
    
    private Set<Integer> getDaysSet(String daystring){
        Set<Integer> daysSet = new Set<Integer>();
        if(daystring != null){
            for(string s: daystring.split(',')){
                if(s.length() > 0){
                    daysSet.add(integer.valueOf(s));
                }
            }
        }
        return daysSet;
    }
    
    public PageReference savePdf(){
        /*String years = ApexPages.currentPage().getParameters().get('year');
    
        pageReference pdfPage = Page.CustomerCalenderPdfDocument;
        pdfPage.getParameters().put('id', customerId);
        pdfPage.getParameters().put('year',String.valueOf(years));
        
        if(!test.isRunningTest()){
        
          BLOB body = pdfPage.getContentAsPdf();
          
          Attachment attachPdf = new Attachment();
          attachPdf.name = customer.Name + ' Year Calender - ' + years+'.pdf';
          attachPdf.body = body;
          attachPdf.ParentId = customerId;
          attachPdf.isPrivate = false;
          
          insert attachPdf;
        }
        
        return new Pagereference('/'+customerId);*/
        return null;
    } 
    
    public class WeekDaysModel{
        public List<DaysModel> dayModels{get;set;}
        public WeekDaysModel(){
            dayModels = new List<DaysModel>();
        }
    }
    
    public class DaysModel{
        public Integer day{get;set;}
        public String styleClas{get;set;}
        public DaysModel(){
        
        }
    }
}