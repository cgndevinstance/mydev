@RestResource(urlMapping='/DragAndDrop/v1/*')
global with sharing class DragAndDropRESTAPI
{
    @HttpPost
    global static String attachDoc(){
        RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;
        
        String fName = req.params.get('FileName');
        String parId = req.params.get('parId');
        String fdesc = req.params.get('desc');
        Blob postContent = req.requestBody; 
        
        Attachment a = new Attachment (ParentId = parId,
                                       Body = postContent,
                                       Name = fName,
                                       //Description='uploaded ' + fdesc=='AL'?'Additional Locations':(fdesc=='PM'?'Principal Members':(fdesc=='LE'?'Legal Entity Type':(fdesc=='FI'?'Finalcial Statements':''))));
                                       Description='uploaded ' + fdesc);
        if(Test.isRunningTest())
            return null;
        insert a;
        return a.Id;
    }
}