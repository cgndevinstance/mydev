public with sharing class VFPageLoginFlowController {
	public string VFPageLoginFlowController() {
		return '/apex/001';
	}
	public PageReference FinishLoginFlowStartUrl() {
        //do stuff
        
        //finish the login flow and send you to the startUrl (account page in this case)
        return Auth.SessionManagement.finishLoginFlow('/001');
    }


    public string FinishLoginFlowHome() {
        //do stuff
        return '/apex/001';
        //finish the login flow and send you the default homepage
        //return Auth.SessionManagement.finishLoginFlow();
    }
}