public class ParsingCSV {
    public static void parseCSV() {
        list<string> headers = new list<string>();
        headers.add('name');
        headers.add('phone');
        headers.add('email');
        headers.add('mobile');
        Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('Contact').getDescribe().getSObjectType();
        sObject thisObj = objectDef.newSobject();
        FieldMapping__c mapping = [select Field_Mapping__c from FieldMapping__c];
        string fieldmapping = mapping.Field_Mapping__c;
        Map<string,string> HeaderApinames = new Map<string,string>();
        List<string> SpltSting = fieldmapping.split(',');
        for ( string str : SpltSting ) {
            string HeaderName = str.substringBefore('=');
            string APIName = str.substringAfter('=');
            HeaderApinames.put(HeaderName,APIName);
        }
        system.debug('HeaderApinamesHeaderApinames'+HeaderApinames);
        for ( string headerName : headers ) {
            if ( HeaderApinames.get(headerName) != null ) {
               if ( headerName == 'name' )
                   thisobj.put(HeaderApinames.get(headerName),'testname');
               else if ( headerName == 'email') 
                   thisobj.put(HeaderApinames.get(headerName),'testname@gmail.com');
               else if ( headerName == 'phone') 
                   thisobj.put(HeaderApinames.get(headerName),'12345');
               else if ( headerName == 'mobile') 
                   thisobj.put(HeaderApinames.get(headerName),'9123456587');
            }
        }
        insert thisobj;
        System.debug('iiiiiiiiiiiiiii'+thisobj.id);
    }
}