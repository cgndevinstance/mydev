public with sharing class CheckingSumExtension {
    public Decimal Number1 { get; set; }
    public Decimal Number2 { get; set; }
    
    public CheckingSumExtension (ApexPages.StandardController controller) {
        Number1 = 0;
        Number2 = 0;
    }
    public Pagereference SaveTheRecord() {
        try {
            Id conId = apexPages.currentPage().getParameters().get ('id');
            contact contactToUpdate = NEW Contact ();
            contactToUpdate.ID = conID;
            
            contactToUpdate.gopinamespace__Amount__c = Decimal.valueOf (apexPages.currentPage().getParameters().get ('sumVal'));
            Update contactToUpdate;
        }
        Catch (Exception e) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage (errorMsg);
            
        }
        return null;
    }
    
}