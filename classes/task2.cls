//  -----------------         using Wrapper class       -----------------------

/*  ClassName   :   Task2
    CreateBy    :   Ch.V.GopiNadh
    Date        :   20/12/2013  
    Description :   creating A button on account record when we click that button all its related contacts 
                    should be displayed in a visualforce page in edit mode.In that add row and 
                    delete row  functionality shold be present */ 



global class task2 {

    public task2(Apexpages.standardController Controller){
            
    }
    public List<myaccount> alist{get;set;}
    public List<contact> clist=new List<contact>();
    public List<contact> deletelist=new List<contact>();
  
    /*
 * Method name      :   getaccounts 
 * Created by       :   Ch.V.Gopinadh
 * Date             :   20/12/2013
 * Description      :   Method which returns the contact details of a current account record
 * Input parameters :   null
 * Returns          :   contact list
 */
 
 
    public PageReference getcontacts(){
        if(alist==null){
            alist = new List<myaccount>();
            
                for(contact c:[select id,lastname,phone,email,accountid from contact where accountid=:apexpages.currentPage().getParameters().get('id')]){
                    
                    alist.add(new myaccount(c));
                
                }
                       
            
        }
        return null;
        
    }
    /*
 * Method name      :   removeitems
 * Created by       :   Ch.V.Gopinadh
 * Date             :   20/12/2013
 * Description      :   Method which delete the contact details of a current account record
 * Input parameters :   null
 * Returns          :   the same page
 */
    
    public PageReference removeitems(){
        for(myaccount m : alist){
            if(m.checked == true){
               deletelist.add(m.con); 
            }
        }
        if(deletelist.size()>0)
            delete deletelist;
        PageReference p = new PageReference('https://ap1.salesforce.com/'+apexpages.currentPage().getParameters().get('id'));
        return p;
    }
     
    /*
 * Method name      :   addrow
 * Created by       :   Ch.V.Gopinadh
 * Date             :   30/12/2013
 * Description      :   Method which calls add() method
 * Input parameters :   null
 * Returns          :   null
 */
    
     
    /*
 * Method name      :   add() 
 * Created by       :   Ch.V.Gopinadh
 * Date             :   30/12/2013
 * Description      :   Method which create a new record and add the row
 * Input parameters :   null
 * Returns          :   null
 */
   public void add(){
        myaccount ma=new myaccount();
        alist.add(ma);
        
    }
    
     
    /*
 * Method name      :   save
 * Created by       :   Ch.V.Gopinadh
 * Date             :   30/12/2013
 * Description      :   Method which save the record that is entered from using addrow functionality
 * Input parameters :   null
 * Returns          :   
 */
    
     public pagereference save(){
        
        for(myaccount m : alist){
            if(m.checked == true){
                m.con.accountId = ApexPages.currentPage().getParameters().get('id');
                clist.add(m.con);
            }
        }
        upsert clist;
        pagereference p = new pagereference('https://ap1.salesforce.com/'+apexpages.currentPage().getParameters().get('id'));
        return p;
    }
    
    public pagereference cancel1(){
        PageReference p = new PageReference('https://ap1.salesforce.com/'+apexpages.currentPage().getParameters().get('id'));
        p.setRedirect(true);
        return p;
    }
   
    public class myaccount{
        
        
        public contact con{get;set;}
        public Integer i;
        public boolean checked{get;set;}
        public myaccount(contact c){
            
            con=c;
            
        }
        public myaccount(){
            con=new contact();
            checked = false;
            
        }
        
    }
    
}
/*  ClassName   :   Task2
    CreateBy    :   Ch.V.GopiNadh
    Date        :   20/12/2013  
    Description :   creating A button on account record when we click that button all its related contacts 
                    should be displayed in a visualforce page in edit mode.In that add row and 
                    delete row  functionality shold be present */ 




//  ---------------    With out using Wrapper class ---------------------------------




/*global class task2 {
    public List<contact> alist{get;set;}
    public List<contact> clist=new List<contact>();
    
   
    public contact con{get;set;}
   
    public task2(Apexpages.standardController Controller){
            
    }*/
    
    /*
 * Method name      :   getaccounts 
 * Created by       :   Ch.V.Gopinadh
 * Date             :   20/12/2013
 * Description      :   Method which returns the contact details of a current account record
 * Input parameters :   null
 * Returns          :   contact list
 */
 
 
 /*  public List<contact> getcontacts(){
        if(alist==null){
            alist = new List<contact>();
            
                for(contact c:[select id,lastname,phone,email,accountid from contact where accountid=:apexpages.currentPage().getParameters().get('id')]){
                    con=c;
                    alist.add(con);
                
                }
                       
            
        }
        return alist;
        
    }*/
    /*
 * Method name      :   removeitems
 * Created by       :   Ch.V.Gopinadh
 * Date             :   20/12/2013
 * Description      :   Method which delete the contact details of a current account record
 * Input parameters :   null
 * Returns          :   the same page
 */
    
 /*   public PageReference removeitems(){
            Id id=ApexPages.currentPage().getParameters().get('pid');
            system.debug('111111111'+id);
            try{
            contact aid =[select id from contact where id=:id];
            delete(aid);
            PageReference p = new PageReference('https://c.ap1.visual.force.com/apex/task2?scontrolCaching=1&id='+apexpages.currentpage().getParameters().get('id'));
            p.setRedirect(true);
            return p;
            }
            catch(System.Exception e){
                e.setMessage('contact is null');
                return null;
            }
            
            
    }*/
     
   
    
     
    /*
 * Method name      :   add() 
 * Created by       :   Ch.V.Gopinadh
 * Date             :   30/12/2013
 * Description      :   Method which create a new record and add the row
 * Input parameters :   null
 * Returns          :   null
 */
  /*  public void add(){
        con=new contact();
        system.debug('addd'+alist);
        alist.add(con);
        system.debug('adddddddddddddddd'+alist);
    }
    public pagereference cancel1(){
        PageReference p = new PageReference('https://ap1.salesforce.com/'+apexpages.currentPage().getParameters().get('id'));
        p.setRedirect(true);
        return p;
    }*/
    
     
    /*
 * Method name      :   save
 * Created by       :   Ch.V.Gopinadh
 * Date             :   30/12/2013
 * Description      :   Method which save the record that is entered from using addrow functionality
 * Input parameters :   null
 * Returns          :   
 */
    
   /*  public pagereference save(){
        /*system.debug('listtttttt'+alist);
        system.debug('size===='+alist.size());
        for(Integer j=0;j<alist.size();j++){
            system.debug('beforeeeeeee'+alist[j]);
            alist[j].accountId = ApexPages.currentPage().getParameters().get('id');
            clist.add(alist[j]);
        }
        system.debug('clist'+clist);
        upsert clist;
        pagereference p = new pagereference('https://ap1.salesforce.com/');
        return p;
    
    for(Integer j=0;j<alist.size();j++){
            
            alist[j].accountId = ApexPages.currentPage().getParameters().get('id');
    }
   upsert alist;
    pagereference p = new pagereference('https://ap1.salesforce.com/');
        return p;
}
}*/