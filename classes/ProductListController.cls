public class ProductListController {
    
    @AuraEnabled
    public static string getProducts(Id oppId) {
        string recordTypeName = [SELECT recordtype.name from opportunity where id=:oppId].recordtype.name;
        recordTypeName = '%'+recordTypeName+'%';
        List<Product2> productList = [SELECT Name,family FROM Product2];// where recordtype.name='Machine' and
                                     // Family like :recordTypeName];
        
        System.debug('productListproductList'+productList);
        set<Id> productIds = new Set<Id>();
        for (product2 prod : productList ) {
            productIds.add(prod.id);
        }
        List<PriceBookEntry> PBEntryList = [select id,Product2Id,UnitPrice from PriceBookEntry where Product2Id in:productIds];
                                                                       //and pricebook2Id ='01s90000001c3y9'];
        Map<Id,PriceBookEntry> ProductPBMap = new Map<Id,PriceBookEntry>();
        for ( PriceBookEntry Pb : PBEntryList ) {
            ProductPBMap.put(Pb.Product2Id,Pb);
        }
        string jsonString = '[';
        for ( Product2 prod : productList) {
            Decimal price = 0;
            if ( ProductPBMap.get(prod.id) != null ) {
                price = ProductPBMap.get(prod.id).UnitPrice;
            }
            jsonString += '{"Name":'+'"'+prod.Name+'",';
            jsonString += '"Family":'+'"'+prod.family+'",';
            jsonString += '"Id":'+'"'+prod.Id+'",';
            jsonString += '"check":'+'"false",';
            jsonString += '"Price":'+'"'+price+'"';
            
            jsonString += '},';
            
        }
        jsonString = jsonString.substring(0,jsonString.length()-1);
        jsonString += ']';
        system.debug('jsonStringjsonString'+jsonString);
        return jsonString;
        
    }
    @AuraEnabled
    public static List<Product2> returnSubProducts(List<Id> selectedProductIds) {
        system.debug('selectedProductIdsselectedProductIds'+selectedProductIds);
        List<Product2> selectedProducts = [select name,Family,ProductScore__c from product2 where id in:selectedProductIds];
        
        return selectedProducts;
    }
    
}