@istest(SeeAllData = True)
private class TechnicalTest_TC{
    static testmethod void method1(){
        TechnicalTest wt = new TechnicalTest();
        
        wt.next();
        wt.skip();
        wt.reset();
        wt.prev();
        wt.start();
        wt.getAnswer();
        wt.setAnswer('A');
        wt.uname = 'TVA1182014005';
        wt.pwd = 'cgn@gmail.com';
        Technical_Questions__c q= new Technical_Questions__c();
        q.Option1__c = 'A';
        q.Option2__c = 'B';
        q.Option3__c = 'A';
        q.Option4__c = 'A';
        q.Option5__c = 'A';
        TechnicalTest.WrapperClassEx we = new TechnicalTest.WrapperClassEx(q,'A');
        we.getAnswers();
        wt.submit();
    }
    static testmethod void method2(){
        TechnicalTest wt = new TechnicalTest();
        Technical_Questions__c q= new Technical_Questions__c();
        q.Option1__c = 'A';
        q.Option2__c = 'B';
        q.Option3__c = 'A';
        q.Option4__c = 'A';
        q.Option5__c = 'A';
        for(Integer i=0;i<=9;i++){
            wt.counterindex = i;
            wt.next();
            wt.skip();
        }
    }
}