public class updatecontactswithcheckboxes {

    public boolean selected{get;set;}
    public List<myaccount> alist{get;set;}
    public List<contact> clist=new List<contact>();
    public updatecontactswithcheckboxes(){
            
    }
    public List<myaccount> getcontacts(){
        if(alist==null){
            alist = new List<myaccount>();
            
                /*for(contact c:[select id,lastname,phone,email,accountid from contact where accountid=:apexpages.currentPage().getParameters().get('id')]){
                    
                    alist.add(new myaccount(c));
                
                }*/
                List<contact> cons = [select id,lastname,phone,email,accountid from contact where accountid=:apexpages.currentPage().getParameters().get('id')];
                for(Contact c:cons)
                    alist.add(new myaccount(c));
                       
            
        }
        return alist;
        
    }
    
    public PageReference removeitems(){
            
            List<contact> selectedlist=new List<contact>();
        for(myaccount mycon:getContacts()){
            
            if(mycon.selected==true){
                selectedlist.add(mycon.con);
                
            }
            
        }
        for(contact con:selectedlist){
            delete con;
        }
        
    
        return null;
            
            
    }
    public pagereference save(){
        List<contact> selectlist = new List<contact>();
        for(myaccount mycon:getContacts()){
            
            if(mycon.selected==true){
                selectlist.add(mycon.con);
                
            }
            
        }
        for(contact con:selectlist){
            update con;
        }
        
        pagereference p = new pagereference('https://ap1.salesforce.com/');
        return p;
    }
   
    public class myaccount{
        
        public boolean selected{get;set;}
        public contact con{get;set;}
        public Integer i;
        public myaccount(contact c){
            
            con=c;
            selected = false;
        }
        public myaccount(){
            con=new contact();
            
            
        }
        
    }
    
}