global class AddRow {
		public List<contact> clist{get;set;}
	    public contact con{get;set;}
	    Id id=apexpages.currentPage().getParameters().get('id');
		public AddRow(Apexpages.standardController Controller){
		    
		}
    
    /*
 * Method name		:	getaccounts  
 * Created by		:	Ch.V.Gopinadh
 * Date				:	5/2/2014
 * Description 		:	Method which returns the contact details of a current account record
 * Input parameters	:	null
 * Returns			:	contact list
 */
 
 
    public List<contact> getcontacts(){
        if( clist == null ){
            clist = new List<contact>();
            
            	for(contact c:[select id,lastname,phone,email,accountid from contact where accountid=:id]){
                	con=c;
                	clist.add(con);
                
            	}
                       
            
        }
        return clist;
        
    }
         
    /*
 * Method name		:	add() 
 * Created by		:	Ch.V.Gopinadh
 * Date				:	5/2/2014
 * Description 		:	Method which create a new record and add the row
 * Input parameters	:	null
 * Returns			:	null
 */
    public void add(){
    	con=new contact();
        clist.add(con);
    	
    }
    public pagereference cancel(){
    	pagereference p= new pagereference('https://c.ap1.visual.force.com/apex/task2?scontrolCaching=1&id=0019000000lOD9I');
    	p.setRedirect(true);
    	return p;
    }
    
     
    /*
 * Method name		:	save
 * Created by		:	Ch.V.Gopinadh
 * Date				:	5/2/2014
 * Description 		:	Method which save the record that is entered from using addrow functionality
 * Input parameters	:	null
 * Returns			:	
 */
    
   public pagereference save(){
    	
    
   	for(Integer j=0;j<clist.size();j++){
    		
   		clist[j].accountId = id;
    }
    upsert clist;
    pagereference p = new pagereference('https://ap1.salesforce.com/'+id);
    p.setRedirect(true);
    return p;
   }
}