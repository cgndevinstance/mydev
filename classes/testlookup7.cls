public class testlookup7{
    public string Obj { get; set; }
    public String val { get; set; }
    public testlookup7(ApexPages.StandardController controller) {
    Obj='';
    }
    public List<SelectOption> getAttachfields()
    {
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('Lead','Lead'));
        fieldNames.add(new SelectOption('Contact','Contact'));
        fieldNames.add(new SelectOption('Account','Account'));
        fieldNames.add(new SelectOption('Opportunity','Opportunity'));
        return fieldNames;
    }
     public void searchresult()
     {
         system.debug('.........................'+Obj);
         if(Obj == 'Lead')
             val = 'Q';
         if(Obj == 'Account')
             val = '1';
         if(Obj == 'Contact')
             val = '3';
         if(Obj == 'Opportunity')
             val = '6';
     }
    
}