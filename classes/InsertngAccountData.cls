public class InsertngAccountData {
    public Account acc{get;set;}
    public InsertngAccountData() {
        acc = new Account();
        List<Account> accList = new List<Account>();
        id accId = apexpages.currentpage().getParameters().get('id');
        accList = [select name,phone from Account where id=:accId];
        if ( accList.size() > 0 ) {
            acc =  accList[0];  
        }
        
    }
    public void insertRecords() {
        
        insert acc;
    }
}