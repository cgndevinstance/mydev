public class PaginationOnProductsController {

   

    private final Integer pageSize = 5;    
     public Integer pageNumber{get;set;}
     public List<Product2> selectedProducts{get;set;}
     public List<Product2> updatelist = new List<Product2>();
     public String str{get;set;}
     
     public  PaginationOnProductsController()
    {
        pageNumber = 0;
    }
    
    public List<pProduct> Productlist
    {
        get
        {
            if ( Productlist == null )
            {
                Productlist = new List<pProduct>();
                for ( Product2 p : [SELECT Id, Name FROM Product2] )
                {   
                    Productlist.add( new pProduct( p ) );
                }
            }
            return Productlist;
        }
        private set;
    }

   

    private List<List<pProduct>> list_Pages
    {
        get
        {
            if ( list_Pages == null )
            {
                list_Pages = new List<List<pProduct>>();
                Integer numInPage = pageSize;
                List<pProduct> thePage;
                for ( pProduct pPro : Productlist )
                {
                    if ( numInPage >= pageSize )
                    {
                        thePage = new List<pProduct>();
                        list_Pages.add( thePage );
                        numInPage = 0;
                    }
                    thePage.add( pPro );
                    numInPage++;
                }
            }
            return list_Pages;
        }
        private set;
    }

    public  List<pProduct> currentPage  { get { return list_Pages[ pageNumber ]; } }

    public  Boolean hasPrevious         { get { return pageNumber > 0; } }
    public  Boolean hasNext             { get { return pageNumber < list_Pages.size() - 1; } }

    public  void previousPage()         { if ( hasPrevious  ) pageNumber--; }
    public  void nextPage()             { if ( hasNext      ) pageNumber++; }

    
     public PageReference updatename() {
        for(Product2 p : selectedProducts){
            //p.id = ApexPages.currentPage().getParameters().get('id');
            //System.debug('iiiiii'+p.id);
            System.debug('nnnnn'+p.name);
            updatelist.add(p);
        }
        update updatelist;
        return null;
    }
    
   public PageReference processSelected()
    {
        
         selectedProducts = new List<Product2>();
        for ( pProduct pPro : Productlist )
        {
            if ( pPro.selected ) {
            selectedProducts.add( pPro.pro );
           }
        }
        /*Cookie sProducts = ApexPages.currentPage().getCookies().get('selectedProducts');
        String s =selectedProducts[0].name;
        if(sProducts==null){
            sProducts = new Cookie('sProducts',s,null,-1,false);
        }
        ApexPages.currentPage().setCookies(new Cookie[]{sProducts});
        Cookie counter = ApexPages.currentPage().getCookies().get('sproducts');
        if(counter != null) {
            System.debug('cccccccccc'+counter.getValue());
            str = counter.getValue();
        }*/
        Pagereference p = new PageReference('/apex/displaySelectedProducts');
        p.setRedirect(false);
        /*for(Integer i=0;i<selectedProducts.size();i++){
           p.getParameters().put('p'+i,string.valueof(selectedProducts[i].Name));
        }*/
        return p;
        
    }

    public class pProduct
    {
        public Product2 pro      { get; set; }
        public Boolean selected { get; set; }
        public pProduct( Product2 p )
        {
            pro         = p;
            selected    = false;
        }
    }
}