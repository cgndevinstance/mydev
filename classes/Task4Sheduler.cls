/*	ClassName	:	Task4Sheduler 
	CreateBy	:	Ch.V.GopiNadh
	Date		:	19/12/2013	
	Description	:	batch apex class to update all contacts with a datetime field to current datetime and 
					schedule it to run for every 1 hour. */ 

global class Task4Sheduler implements schedulable { 
	
	/*
 *  Method name		:	execute 
 * Created by		:	Ch.V.Gopinadh
 * Date				:	19/12/2013
 * Description 		:	Method which calls the Task4BatchClass executeBatch() method
 * Input parameters	:	Database.BatchableContext BC
 * Returns			:	nill
 */
	
	public void execute(SchedulableContext SC){
		
		Task4batchclass tb=new Task4batchclass();
		system.debug('1111111111'+tb);
		ID id = Database.executeBatch(tb,10);
		
	}
}