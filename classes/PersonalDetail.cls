public class PersonalDetail {
    
    private Apexpages.StandardController controller;
    public PersonalDetail(ApexPages.StandardController stdcontroller) {
        this.controller = stdController;
    }
    
    public PageReference save() {
        this.controller.save();
        PageReference newPage = New PageReference('/apex/EducationDetail');
        newPage.getParameters().put('id',ApexPages.currentPage().getParameters().get('id'));
        newPage.setRedirect(true);
        return newPage;
    }

}