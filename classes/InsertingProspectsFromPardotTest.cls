@IsTest
public class InsertingProspectsFromPardotTest{
    public static testMethod void testBatch() {
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        List<Pardot_Credentials__c> credlist = new List<Pardot_Credentials__c>();
        Pardot_Credentials__c pCred = new Pardot_Credentials__c();
        pCred.email__c = 'syed@mysalesforcecrm.com';
        pCred.password__c = 'Tvarana@1234';
        pCred.user_id__c = '45fdaac4a9338e31032862eebebfa8e0';
        credlist.add(pCred);
        insert credlist;
        InsertingProspectsFromPardot insertprospects = new InsertingProspectsFromPardot(0,null);
        QL = insertprospects.start(BC);
        Test.startTest();
          //insertprospects.execute(BC,credlist);  
          Test.setMock(HttpCalloutMock.class, new  ApikeyTet()); 
          insertprospects.execute(bc,credlist);
          insertprospects.finish(bc);
        Test.stopTest();
    }
}