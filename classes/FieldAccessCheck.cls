public with sharing class FieldAccessCheck {

    public FieldAccessCheck(ApexPages.StandardController controller) {

    }

    public String accName{get;set;}
    
    public void fieldAccessMethod() {
        //List<Contact> conList = [SELECT Name, gopinamespace__Account_Name__c FROM Contact where id='0032v00003gLpIq'];
        //System.debug('conList'+conList[0].gopinamespace__Account_Name__c);
        Contact con = new Contact();
        con.id = '0032v00003gLpIq';
        con.gopinamespace__Account_Name__c = accName;
        con.lastname = 'test account update'+system.now();
        update con;
    }
}