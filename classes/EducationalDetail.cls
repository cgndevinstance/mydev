public class EducationalDetail {
    
   public Education_Detail__c detail;
    public Login__c l;
    List<Education_Detail__c> ed = new List<Education_Detail__c>();
    public Education_Detail__c email{
       get; private set;
    }
    private Apexpages.StandardController stdcontroller;
    
    public EducationalDetail(ApexPages.StandardController controller) {
        this.stdcontroller = controller;
        email=(Education_Detail__c)controller.getRecord();
    }
    
    public Education_Detail__c getDetail() {
        if(detail == null) {
            detail = new Education_Detail__c();
        }
        return detail;
    }
    
    public PageReference step2() {
         ed = [select email__c from Education_Detail__c where email__c =:email.Email__c];
         if(ed.size()>0){
            ApexPages.message msg = new ApexPages.message(ApexPages.severity.ERROR,'Email already Exist');
            Apexpages.addMessage(msg); 
            return null;
         }
         else        
            return page.EducationDetail2;
    }
    public PageReference prev(){
        return page.EducationDetail2;
    }
    
    public PageReference step1() {
        return Page.EducationDetail;
    }
    public PageReference step3() {
        return Page.Miscellaneous;
    }
    
    public PageReference step4() {
                
          return Page.EducationDetailConfirmation;
    }
    
    public PageReference generatepdf() {
        this.stdcontroller.save();
        return Page.GeneratePdf;
    }
    
    public PageReference submit() {
        this.stdcontroller.save();
        
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {email.Email__c}; 
                system.debug('emailllll'+email.Email__c);
                mail.setToAddresses(toAddresses);
                mail.setSenderDisplayName('Gmail');
                mail.setSubject('Registration');
                String str='Thank you for your interest.!';
                mail.setPlainTextBody(str);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                l=new Login__c();
                l.password__c = email.Email__c;
                Education_Detail__c e = [select Id,name from Education_Detail__c where email__c =:email.Email__c];
                l.Education_Detail__c = e.Id;
                
                insert l;
        return Page.thankyou;
   } 
}