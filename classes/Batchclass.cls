global class Batchclass implements database.Batchable<contact> {
	public string query;
	string email;
	public iterable<contact> start(Database.BatchableContext BC){
		contact[] c=[select id,email from contact];
		return c;
	}
	public void execute(Database.BatchableContext BC, list<contact> scope){
			
		
	}
	public void finish(Database.BatchableContext BC){
		
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new String[] {'gopinath@tvarana.com'});
		
		mail.setSenderDisplayName('Batch Processing');
		mail.setSubject('Batch Process Completed');
		
		mail.setPlainTextBody('Batch Process has completed');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}

}