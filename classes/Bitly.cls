global class Bitly {
    Public String mode;
    Public String sUrl;
    public String getbitly () {
        String shorten;
        XmlStreamReader reader;
        HttpResponse res;
        
        Http h = new Http();
        HttpRequest req = buildWebServiceRequest(sURL);
        
        
        if (mode=='live') { res = invokeWebService(h, req); }
        
        if (mode=='live') { reader = res.getXmlStreamReader(); }
        else
        {
            String str = '<bitly><results shortUrl="http://bit.ly/QqHEm">Foo bar</results></bitly>';
            reader = new XmlStreamReader(str);
        }
        return readXMLResponse(reader,'shortUrl');
    
    }
    
    public static HttpRequest buildWebServiceRequest(String purl){
        String endpoint;
        HttpRequest req = new HttpRequest();//R_948fa681da46221f969e83b2ba52d31e R_5db3441b93014d9aacdd120a6c7b9a03
        //endpoint = 
        endpoint = 'http://api.bit.ly/shorten?version=2.0.1&format=xml&history=1&longUrl=' + purl + '&login=chvgopinadh&apiKey=R_aec350822f7e4c0689cc69c5b487af9f';
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        return req;
    }
    public static HttpResponse invokeWebService(Http h, HttpRequest req){
        HttpResponse res = h.send(req);
        return res;
   }
    
    public static String readXMLResponse(XmlStreamReader reader, String sxmltag){
        string retValue;
        // Read through the XML
        system.debug(reader.toString());
        while(reader.hasNext()) {
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
                if (reader.getLocalName() == sxmltag) {
                    reader.next();
                    if (reader.getEventType() == XmlTag.characters) {
                        retValue = reader.getText();
                    }
                }
            }
        reader.next();
        }
        return retValue;
    }

}