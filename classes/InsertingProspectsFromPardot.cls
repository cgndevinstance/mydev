global class InsertingProspectsFromPardot implements Database.Batchable<sObject> ,Database.Stateful,Database.AllowsCallouts 
{
    Global String email;
    Global String api_key;
    Global String user_id;
    Global String password;
    Global String endpoint;
    Global String xmlString;
    Global String finalvalue;
    Global String endpointForXML;
    Global String totalNumberOfPardotProspects;
    
    Global Integer pos1;
    Global Integer pos2;
    Global Integer offsetValue;
    
    Global String[] xmlToString;
    Global String[] xmlToStringforTotalValues;
    
    Global Set<ID> allProspectIds = new Set<ID>();
    Global Set<ID> allCampIDs = new Set<ID>();
   
    Global List<Lead> allprospects = new List<Lead>();
    Global List<CampaignMember> allCampaignMembers = new List<CampaignMember>();
    Global List<Campaign> allCampaigns = new List<Campaign>();
    
    public InsertingProspectsFromPardot(Integer offset,Set<ID> leadIdsFromPardot){
    
        offsetValue = offset;
        if(leadIdsFromPardot != NULL)
            allProspectIds.addAll(leadIdsFromPardot);
        
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query= 'select Email__c,password__c,user_id__c from Pardot_Credentials__c where Email__c!=NULL';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Pardot_Credentials__c> credentials)
    {
        email = credentials[0].Email__c;
        password = credentials[0].password__c;
        user_id = credentials[0].user_id__c;
        
        endpoint = 'https://pi.pardot.com/api/login/version/3?email='+email+'&password='+password+'&user_key='+user_id ;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        Http h = new Http();
        HttpResponse res = h.send(req);
        
        Dom.Document doc = res.getBodyDocument();
        Dom.XMLNode address = doc.getRootElement();
        api_key = address.getChildElement('api_key', null).getText();
        
        endpointForXML = 'https://pi.pardot.com/api/prospect/version/3/do/query?user_key='+user_id+'&api_key='+api_key+'&offset='+offsetValue;
        HttpRequest reqForXML = new HttpRequest();
        reqForXML.setEndpoint(endpointForXML);
        reqForXML.setMethod('POST');
        Http httpForXML = new Http();
        HttpResponse responseOfXML = httpForXML.send(reqForXML);
        xmlString = responseOfXML.getBody();
        xmlToStringforTotalValues = xmlString.split('<result>');
        
       /* allCampaigns = [SELECT ID,Name FROM Campaign WHERE Name != NULL];
        for(campaign camp:allCampaigns){
            allCampIDs.add(camp.id);
        }*/
        
        for(integer i=1; i<xmlToStringforTotalValues.size();i++) {
    
            xmlToStringforTotalValues[i] = 'appendString'+xmlToStringforTotalValues[i];
            totalNumberOfPardotProspects = gettingValuesFromXML('<total_results>','</total_results>',xmlToStringforTotalValues[i]);
        }
         
        xmlToString = xmlString.split('<prospect>');
        
        for(integer i=1; i<xmlToString.size();i++) {
    
            xmlToString[i] = 'appendString'+xmlToString[i];
            Lead newLead = new Lead();
            
            newLead.salutation = gettingValuesFromXML('<salutation>','</salutation>',xmlToString[i]);
            newLead.firstName = gettingValuesFromXML('<first_name>','</first_name>',xmlToString[i]);
            newLead.lastName = gettingValuesFromXML('<last_name>','</last_name>',xmlToString[i]);
            newLead.email = gettingValuesFromXML('<email>','</email>',xmlToString[i]);
            newLead.company = gettingValuesFromXML('<company>','</company>',xmlToString[i]);
           
            allProspects.add(newLead);
        
        }
        
        if( allProspects.size()> 0 ) {
            insert allProspects;
            
          /*  for(lead l:allProspects){
                 CampaignMember campaignmemberToAdd = new CampaignMember();
                 campaignmemberToAdd.CampaignId = l.Campaign_ID__c;
                 campaignmemberToAdd.leadid =  l.id ;
                 allCampaignMembers.add(campaignmemberToAdd);
                 allProspectIds.add(l.Id);
           }
            insert allCampaignMembers; */
            
        }
       
    }
    Global String gettingValuesFromXML(String startNode,String endNode,String mainString){
        
        pos1 = mainString.indexOf(startNode);
        if(pos1==-1)
            return 'N/A';
        pos2 = mainString.indexOf(endNode);
        if(pos2==-1)
            return 'N/A';
        finalValue = mainString.subString(pos1+startNode.length(),pos2);
        if(finalValue == '')
            return 'N/A';
        
        return finalValue;
        
   }
     
    global void finish(Database.BatchableContext BC)
    {  
         
        if( offsetValue <= Integer.ValueOf(totalNumberOfPardotProspects))
            Database.executeBatch(new InsertingProspectsFromPardot((offsetValue+200),allProspectIds));
        else
            Database.executeBatch(new UpdatePardotDateOfProspect(allProspectIds),9);
        
    }
}