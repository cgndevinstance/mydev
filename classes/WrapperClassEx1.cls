public class WrapperClassEx1{ 
    public WrapperClassEx1(ApexPages.StandardSetController controller) {

    }


    public WrapperClassEx1(ApexPages.StandardController controller) {

    }


    public List<contact> conList{get;set;}
    Public List<Wrapper> wrapList{get;set;}
    public WrapperClassEx1() {
        
    }
    public void contactListMethod () {
        wrapList = new List<Wrapper>();
        conList = [select Name,phone,Email,AccountId from contact ];
        for ( contact con: conList ) {
            wrapList.add(new wrapper(con));
        }
        
        
    }
    public pagereference prcessSelected() {
        List<contact> deleteContacts = new List<contact>();
        for ( wrapper wrap : wrapList ) {
            if ( wrap.selected == true ) {
                Contact con = wrap.con;
                deleteContacts.add(con);
            }
        }
        delete deleteContacts;
        return null;
    }
    public class Wrapper {
        
        public Contact con{get;set;}
        public Boolean selected{get;set;}
        public wrapper(Contact con) {
            this.con = con;
            selected = False;
        }
    }
}