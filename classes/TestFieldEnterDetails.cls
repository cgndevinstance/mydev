@isTest
public with sharing class TestFieldEnterDetails {
	static testmethod void testDetails(){
	 	Account a = new Account();
	 	Apexpages.StandardController stdcontroller = new Apexpages.StandardController(a);
	 	FieldEnterDetails f = new FieldEnterDetails(stdcontroller);
	 	f.getData();
	 	f.secondPage();
	 	f.thirdpage();
	 	f.submit();
	 	f.modify();
	 	f.back();
	}
}