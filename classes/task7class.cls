/*  ClassName   :   Task7Class 
    CreateBy    :   Ch.V.GopiNadh
    Date        :   20/12/2013  
    Description :   to display all accounts in a single row with edit and delete links */ 
    
public class task7class {
    public List<myaccount> alist {get;set;}
    
    
    /*
 *  Method name     :   getaccounts 
 * Created by       :   Ch.V.Gopinadh
 * Date             :   20/12/2013
 * Description      :   Method which returns the contact details of a selected account record
 * Input parameters :   null
 * Returns          :   contact list
 */
 
    public List<myaccount> getAccounts(){
        if(alist==null){
            alist = new List<myaccount>();
            for(account a:[select id,name,accountnumber,industry from account]){
                for(contact c:[select id,name,phone,email,accountid from contact where accountid=:a.id]){
                    
                    alist.add(new myaccount(a,c));
                
                }
            }
            
            
        }
        return alist;
        
    }
    
    
    /*
 *  Method name     :   removeitems 
 * Created by       :   Ch.V.Gopinadh
 * Date             :   20/12/2013
 * Description      :   Method which removes the account record
 * Input parameters :   null
 * Returns          :   contact list
 */
 
 
    public PageReference removeitems(){
            
            //ac=[select id from account];
            //delete(ac);
            Account aid =[select id from account where id=:ApexPages.currentPage().getParameters().get('pid')];
            system.debug('...........'+aid);
            delete(aid);
            PageReference p = new PageReference('https://c.ap1.visual.force.com/apex/task7page');
            
            p.setRedirect(true);
            return p;
    }
    
    /*ClassName :   myaccount
    CreateBy    :   Ch.V.GopiNadh
    Date        :   20/12/2013  
    Description :   wrapper class which contains both accounts and contacts */
   
    public class myaccount{
        
        public account acc{get;set;}
        public contact con{get;set;}
        
        public myaccount(account a,contact c){
            
            con=c;
            acc=a;
        }
    }
    
}