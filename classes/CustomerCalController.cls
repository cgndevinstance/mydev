global class CustomerCalController{
    //Date variables 
    public Date rescheduleDate{get;set;}
    public Date firstDate{get;set;}
    private Date presentFirstDate{get;set;}
   
    //String variables
    public string EventBody{get;set;}
    public String EventBody1{get;set;}
    public String EventBody2{get;set;}
    public String WorkOrderId{get;set;}
    public String worderid{get;set;}
    public String woid{get;set;}
    public String notebody{get;set;}
    public String monthyear{get;set;}
    public String ischeck;
    public String rescheduleReason{get;set;}
    public String additionalFee{get;set;}
    public String CustomerId{get;set;}
    public string locationCode{get;set;}
    public String cancellationReason{get;set;}
    
    //Integer variables
    public Integer newday{get;set;}
    public Integer dayy{get;set;}
    public Integer monthh{get;set;}
    public Integer yearr{get;set;}
    
    public Boolean isDaily{get;set;}
    public Boolean isApplicable{get;set;}
    
    //Object variables
    public Service_Work_Order__c serviceOrder{get;set;}
    
    //Lists and Sets
    public List<string> dateCalendar{get;set;}
    public List<Note> workOrderNotes{get;set;}
    public Set<Date> allEventsDates{get;set;}
    
    //Maps
    private Map<String, Service_Work_Order__c> worderMap;
    private Map<String, Integer> monthMap;
    public Map<string,list<Note>> WoNotesMap{get;set;}
    public Map<String,List<Work_Order_Room__c>> WoRoomsMap{get;set;}
    public Map<String,List<Work_Order_Service__c>> WoServicesMap{get;set;}
    
    public string RecName{get;set;}
    //Constructor
    public CustomerCalController(){
        //intilaizing
        serviceOrder = new Service_Work_Order__c();
        dateCalendar = new list<string>();
        worderMap = new Map<String, Service_Work_Order__c>();
        workOrderNotes = new List<Note>();
        
        //Getting page parameters        
        String dy = apexPages.currentPage().getParameters().get('dy');
        String mon = apexPages.currentPage().getParameters().get('mn');
        String yr = apexPages.currentPage().getParameters().get('yr');
        ischeck = apexPages.currentPage().getParameters().get('ischk');
        
        //map
        monthMap = new Map<String, Integer>();
        monthMap.put('January',1);
        monthMap.put('February',2);
        monthMap.put('March',3);
        monthMap.put('April',4);
        monthMap.put('May',5);
        monthMap.put('June',6);
        monthMap.put('July',7);
        monthMap.put('August',8);
        monthMap.put('September',9);
        monthMap.put('October',10);
        monthMap.put('November',11);
        monthMap.put('December',12);
        System.debug('ddddddddddddddd'+dy);
        system.debug('mmmmmmmmmmm'+mon);
        system.debug('yyyyyyyyyyyy'+yr);
        System.debug('iiiiiiiiiii'+ischeck);
        dateCalendar.add(dy);
        dateCalendar.add(mon);
        dateCalendar.add(yr);
        
        dayy = integer.valueof(dy);
        monthh = integer.valueof(mon);
        yearr = integer.valueof(yr);
        
        fetchWorkOrderRecords(integer.valueof(dy),integer.valueof(mon),integer.valueof(yr));
    }
    
    public void fetchWorkOrderRecords(integer day, integer month, integer year){
        
        WoNotesMap = new map<string,list<Note>>();
        WoRoomsMap = new Map<String,List<Work_Order_Room__c>>();
        WoServicesMap = new Map<String,List<Work_Order_Service__c>>();
        string idd = apexPages.currentPage().getParameters().get('id');
        List<Service_Work_Order__c> workOrderList = new List<Service_Work_Order__c>();
        List<Service_Work_Order__c> workOrderList1 = new List<Service_Work_Order__c>();
        List<Service_Work_Order__c> workOrderList2 = new List<Service_Work_Order__c>();
        
        if(idd != null && idd.length()>0){
            
            list<account> cusList = [select id,Location__r.Location_Code__c from account where id=: idd limit 1];
            
            if(cusList.size()>0)
                locationCode = cusList[0].Location__r.Location_Code__c;
            
            FindEventDates(idd);
            if(ischeck == 'true'){
                workOrderList = getWorkOrdersList(year, month, idd);
                
                if(month == 12){
                    month = 0;
                    year = year + 1;
                }
                
                workOrderList1 = getWorkOrdersList(year, month+1, idd);
                
                if((month+1) == 12){
                    month = -1;
                    year = year + 1;
                }
                
                workOrderList2 = getWorkOrdersList(year, month+2, idd);
                
            }else{
                workOrderList = getNonCancelledWorkOrdersList(year, month, idd);
                
                if(month == 12){
                    month = 0;
                    year = year + 1;
                }
                workOrderList1 = getNonCancelledWorkOrdersList(year, month+1, idd);
                
                if((month+1) == 12){
                    month = -1;
                    year = year + 1;
                }
                workOrderList2 = getNonCancelledWorkOrdersList(year, month+2, idd);
            }
            system.debug('workOrderListworkOrderList'+workOrderList);
            if(workOrderList != null && workOrderList.size()>0){
                Set<Date> WOServiceDateSet = new Set<Date>();
                FindNotes(workOrderList);
                FindWorkOrderRooms(workOrderList);
                FindWorkOrderServices(workOrderList);
                EventBody = ' ';
                Boolean editable = false;
                
                
                for(Service_Work_Order__c rec: workOrderList){
                    WOServiceDateSet.add(rec.Service_Date__c);
                    String idString = String.valueOf(rec.Service_Date__c.day())+String.valueOf(rec.Service_Date__c.month())+String.valueOf(rec.Service_Date__c.year());
                    worderMap.put(rec.id,rec);
                    EventBody += '{';
                    EventBody += 'id : '+rec.Service_Date__c.day()+rec.Service_Date__c.month()+rec.Service_Date__c.year()+', ';
                    EventBody += 'recid : \''+rec.id+'\'  , ';
                   /* if(rec.Status__c == 'Cancelled'){
                      EventBody += 'title : \''+rec.Rescheduled_Reason__c+'\'  , ';
                    }else if(rec.Work_Team__r.Captain__r.Name != null && rec.Status__c == 'Final'){
                        String title = rec.Work_Team__r.Captain__r.Name.replaceAll('\'',' ')+'(F)';
                        EventBody += 'title : \''+title+'\'  , ';
                    }else if(rec.Work_Team__r.Captain__r.Name != null){
                        String title = rec.Work_Team__r.Captain__r.Name.replaceAll('\'',' ');
                        EventBody += 'title : \''+title+'\'  , ';
                    }else if(rec.Status__c == 'Final'){
                        EventBody += 'title :\'UnAssigned(F) \', ';
                    }else{
                        EventBody += 'title :\'UnAssigned \', ';
                    }*/
                    EventBody += 'title : \''+rec.name+'\'  , ';
                    
                    EventBody += 'start : '+'new Date('+rec.Service_Date__c.year()+', '+(rec.Service_Date__c.month()-1)+', '+rec.Service_Date__c.day()+')';
                    if(rec.Service_Date__c < Date.Today() || rec.Status__c == 'Cancelled' || rec.Status__c == 'Final'){
                        EventBody += ', editable :'+ editable +' ';
                    }
                    if(rec.Status__c == 'Cancelled'){
                        EventBody += ', color : \'#DFDFDF\' ';
                    }
                    else if(rec.Status__c != 'Scheduled' && rec.Status__c != 'Cancelled'){
                        EventBody += ', color : \'#7ac142\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && allEventsDates.contains(rec.Service_Date__c)){
                        EventBody += ', color : \'#893BFF\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && WoRoomsMap.containsKey(rec.id)){
                        EventBody += ', color : \'#FF8789\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && WoServicesMap.containsKey(rec.id)){
                        EventBody += ', color : \'#2cabd3\' ';
                    }
                        
                    EventBody += '},'; 
                }
                
                for(Date dte : allEventsDates){
                    if(!WOServiceDateSet.contains(dte)){
                            EventBody += '{';
                            EventBody += 'title :\'Holiday\', ';
                            EventBody += 'start : \''+ dte +'\'  , ';
                            EventBody += 'color : \'yellow\', ';
                            EventBody += 'editable :'+ editable +' ';
                            EventBody += '},'; 
                    }
                }
                
                if(EventBody != null && EventBody.length()>0){
                    EventBody = EventBody.substringBeforeLast(',');
                }
                System.debug('EventBodyEventBody'+EventBody);
            }   
            
            /*if(workOrderList1 != null && workOrderList1.size()>0){
                Set<Date> WOServiceDateSet1 = new Set<Date>();
                FindNotes(workOrderList1);
                FindWorkOrderRooms(workOrderList1);
                FindWorkOrderServices(workOrderList1);
                EventBody1 = ' ';
                Boolean editable = true;
                
                for(Service_Work_Order__c rec: workOrderList1){
                    WOServiceDateSet1.add(rec.Service_Date__c);
                    String idString = String.valueOf(rec.Service_Date__c.day())+String.valueOf(rec.Service_Date__c.month())+String.valueOf(rec.Service_Date__c.year());
                    worderMap.put(idString,rec);
                    EventBody1 += '{';
                    EventBody1 += 'id : '+rec.Service_Date__c.day()+rec.Service_Date__c.month()+rec.Service_Date__c.year()+', ';
                    
                    if(rec.Status__c == 'Cancelled'){
                      EventBody1 += 'title : \''+rec.Rescheduled_Reason__c+'\'  , ';
                    }else if(rec.Work_Team__r.Captain__r.Name != null && rec.Status__c == 'Final'){
                        String title = rec.Work_Team__r.Captain__r.Name.replaceAll('\'',' ')+'(F)';
                        EventBody1 += 'title : \''+title+'\'  , ';
                    }else if(rec.Work_Team__r.Captain__r.Name != null){
                        String title = rec.Work_Team__r.Captain__r.Name.replaceAll('\'',' ');
                        EventBody1 += 'title : \''+title+'\'  , ';
                    }else if(rec.Status__c == 'Final'){
                        EventBody1 += 'title :\'UnAssigned(F) \', ';
                    }else{
                        EventBody1 += 'title :\'UnAssigned \', ';
                    }
                    
                    EventBody1 += 'start : '+'new Date('+rec.Service_Date__c.year()+', '+(rec.Service_Date__c.month()-1)+', '+rec.Service_Date__c.day()+')';
                    if(rec.Service_Date__c < Date.Today() || rec.Status__c == 'Cancelled' || rec.Status__c == 'Final'){
                        EventBody1 += ', editable :'+ editable +' ';
                    }
                    if(rec.Status__c == 'Cancelled'){
                        EventBody1 += ', color : \'#DFDFDF\' ';
                    }
                    else if(rec.Status__c != 'Scheduled' && rec.Status__c != 'Cancelled'){
                        EventBody1 += ', color : \'#7ac142\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && allEventsDates.contains(rec.Service_Date__c)){
                        EventBody1 += ', color : \'#893BFF\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && WoRoomsMap.containsKey(rec.id)){
                        EventBody1 += ', color : \'#FF8789\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && WoServicesMap.containsKey(rec.id)){
                        EventBody1 += ', color : \'#2cabd3\' ';
                    }
                        
                    EventBody1 += '},'; 
                }
                
                for(Date dte : allEventsDates){
                    if(!WOServiceDateSet1.contains(dte)){
                        EventBody1 += '{';
                        EventBody1 += 'title :\'Holiday\', ';
                        EventBody1 += 'start : \''+ dte +'\'  , ';
                        EventBody1 += 'color : \'yellow\', ';
                        EventBody1 += 'editable :'+ editable +' ';
                        EventBody1 += '},'; 
                    }
                }
                
                if(EventBody1 != null && EventBody1.length()>0){
                    EventBody1 = EventBody1.substringBeforeLast(',');
                }
                System.debug('EventBodyEventBody1'+EventBody1);
                System.debug('worderMapworderMap'+worderMap);
            } */
            
            if(workOrderList2 != null && workOrderList2.size()>0){
                Set<Date> WOServiceDateSet2 = new Set<Date>();
                FindNotes(workOrderList2);
                FindWorkOrderRooms(workOrderList2);
                FindWorkOrderServices(workOrderList2);
                EventBody2 = ' ';
                Boolean editable = false;
                
                for(Service_Work_Order__c rec: workOrderList2){
                    WOServiceDateSet2.add(rec.Service_Date__c);
                    String idString = String.valueOf(rec.Service_Date__c.day())+String.valueOf(rec.Service_Date__c.month())+String.valueOf(rec.Service_Date__c.year());
                    //worderMap.put(idString,rec);
                    EventBody2 += '{';
                    EventBody2 += 'id : '+rec.Service_Date__c.day()+rec.Service_Date__c.month()+rec.Service_Date__c.year()+', ';
                    
                    if(rec.Status__c == 'Cancelled'){
                      EventBody2 += 'title : \''+rec.Rescheduled_Reason__c+'\'  , ';
                    }else if(rec.Work_Team__r.Captain__r.Name != null && rec.Status__c == 'Final'){
                        String title = rec.Work_Team__r.Captain__r.Name.replaceAll('\'',' ')+'(F)';
                        EventBody2 += 'title : \''+title+'\'  , ';
                    }else if(rec.Work_Team__r.Captain__r.Name != null){
                        String title = rec.Work_Team__r.Captain__r.Name.replaceAll('\'',' ');
                        EventBody2 += 'title : \''+title+'\'  , ';
                    }else if(rec.Status__c == 'Final'){
                        EventBody2 += 'title :\'UnAssigned(F) \', ';
                    }else{
                        EventBody2 += 'title :\'UnAssigned \', ';
                    }
                    
                    EventBody2 += 'start : '+'new Date('+rec.Service_Date__c.year()+', '+(rec.Service_Date__c.month()-1)+', '+rec.Service_Date__c.day()+')';
                    if(rec.Service_Date__c < Date.Today() || rec.Status__c == 'Cancelled'  || rec.Status__c == 'Final'){
                        EventBody2 += ', editable :'+ editable +' ';
                    }
                    if(rec.Status__c == 'Cancelled' ){
                        EventBody2 += ', color : \'#DFDFDF\' ';
                    }
                    else if(rec.Status__c != 'Scheduled' && rec.Status__c != 'Cancelled'){
                        EventBody2 += ', color : \'#7ac142\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && allEventsDates.contains(rec.Service_Date__c)){
                        EventBody2 += ', color : \'#893BFF\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && WoRoomsMap.containsKey(rec.id)){
                        EventBody2 += ', color : \'#FF8789\' ';
                    }
                    else if(rec.Status__c == 'Scheduled' && WoServicesMap.containsKey(rec.id)){
                        EventBody2 += ', color : \'#2cabd3\' ';
                    }
                    /*else if(WoNotesMap.containsKey(rec.id)){
                        EventBody2 += ', color : \'yellow\' ';
                    }*/
                        
                    EventBody2 += '},'; 
                }
                
                for(Date dte : allEventsDates){
                    if(!WOServiceDateSet2.contains(dte)){
                        EventBody2 += '{';
                        EventBody2 += 'title :\'Holiday\', ';
                        EventBody2 += 'start : \''+ dte +'\'  , ';
                        EventBody2 += 'color : \'yellow\', ';
                        EventBody2 += 'editable :'+ editable +' ';
                        EventBody2 += '},'; 
                    }
                }
                
                if(EventBody2 != null && EventBody2.length()>0){
                    EventBody2 = EventBody2.substringBeforeLast(',');
                }
                System.debug('EventBodyEventBody2'+EventBody2);
            }                                   
        } 
    }
    
    //method return list of workorder by month,year and customer
    private List<Service_Work_Order__c> getWorkOrdersList(Integer year, Integer month, String custId){
    System.debug('yyyyyyyyyyyyy'+year);
    System.debug('mmmmmmmmmmmm'+month);
    System.debug('ccccccccccccc'+custId);
    
        List<Service_Work_Order__c> workOrders = new List<Service_Work_Order__c>();
        List<Service_Work_Order__c> tempWorkOrders = new List<Service_Work_Order__c>();
        tempWorkOrders = [SELECT per_Qtr__c, Work_Team__c, Visit_Order__c,name,
                          Work_Team__r.Deleted__c, Work_Team__r.Captain__r.Name,status__c, Work_Team__r.Captain__c, Service_Agreement__r.Customer__c,Service_Date__c
                          FROM Service_Work_Order__c
                         /* WHERE CALENDAR_YEAR(Service_Date__c) =: year
                          AND CALENDAR_MONTH(Service_Date__c) =: month
                          AND Service_Agreement__r.Customer__c =: custId
                          order by Service_Date__c ASC */
                           ];
        
        if(tempWorkOrders != null && tempWorkOrders.size() > 0){
            workOrders = tempWorkOrders;
        }
        return workOrders;
    }
    
    //method return list of workorder by month,year and customer
    private List<Service_Work_Order__c> getNonCancelledWorkOrdersList(Integer year, Integer month, String custId){
        List<Service_Work_Order__c> workOrders = new List<Service_Work_Order__c>();
        List<Service_Work_Order__c> tempWorkOrders = new List<Service_Work_Order__c>();
        tempWorkOrders = [SELECT per_Qtr__c, Work_Team__c, Visit_Order__c
                          FROM Service_Work_Order__c 
                          WHERE CALENDAR_YEAR(Service_Date__c) =: year 
                          AND CALENDAR_MONTH(Service_Date__c) =: month
                          AND Service_Agreement__r.Customer__c =: custId 
                          AND Status__c != 'cancelled'
                          order by Service_Date__c ASC];
        
        if(tempWorkOrders != null && tempWorkOrders.size() > 0){
            workOrders = tempWorkOrders;
        }
        return workOrders;
    }
    
    // method for drap and drop functionality    
    public void updateSworder(){
        system.debug('worderidworderid'+worderid);
        system.debug('monthyearmonthyear'+monthyear);
        System.debug('newdaynewday'+newday);
        /*if(worderid != null && worderid.length() > 0 && monthyear != null && monthyear.length() > 0 && newday != null){
            Date newDate;
            List<String> iddateList = worderid.split('=');
            List<String> newdatelist = monthyear.split(' ');
            if(iddatelist != null && iddatelist.size() == 2){
                List<String> spliteddate = iddatelist[1].split('-');
                if(spliteddate != null && spliteddate.size() > 0){
                    if(spliteddate[0] != 'NaN'){
                        newDate = Date.newInstance(Integer.valueOf(spliteddate.get(2).trim()),Integer.valueOf(spliteddate.get(1).trim()),Integer.valueOf(spliteddate.get(0).trim()));
                    }else{
                        if(newdatelist != null && newdatelist.size() > 0){
                            newDate = Date.newInstance(Integer.valueOf(newdatelist.get(1).trim()),Integer.valueOf(monthMap.get(newdatelist.get(0).trim())),newday);
                        }
                    }
                }else{
                    if(newdatelist != null && newdatelist.size() > 0){
                        newDate = Date.newInstance(Integer.valueOf(newdatelist.get(1).trim()),Integer.valueOf(monthMap.get(newdatelist.get(0).trim())),newday);
                    }
                }
            }else{
                if(newdatelist != null && newdatelist.size() > 0){
                    newDate = Date.newInstance(Integer.valueOf(newdatelist.get(1).trim()),Integer.valueOf(monthMap.get(newdatelist.get(0).trim())),newday);
                }
            }
            List<Service_Work_Order__c> sworder = new List<Service_Work_Order__c>();
            sworder.add(worderMap.get(iddateList.get(0)));
            if(sworder.get(0) != null && sworder.size() > 0){
                sworder.get(0).Service_Date__c = newDate;
                sworder.get(0).Work_Team__c = null;
                try{
                    update sworder; 
                }
                catch(exception e){
                    system.debug('## Error : update service work order ==> '+e.getMessage());   
                }       
            }
        } */
        List<Service_Work_Order__c> sworder = new List<Service_Work_Order__c>();
        sworder.add(worderMap.get(worderid));
        List<String> newdatelist = monthyear.split(' ');
        System.debug('newdatelistnewdatelist'+newdatelist);
        System.debug('monthyearmonthyear'+monthyear);
        system.debug('RecNameRecName'+RecName);
        Date newDate = Date.newInstance(Integer.valueOf(newdatelist.get(1).trim()),Integer.valueOf(newdatelist.get(0).trim()),newday);
        if(sworder.size() > 0){
                sworder[0].Service_Date__c = newDate;
                sworder[0].Work_Team__c = null;
                try{
                    update sworder; 
                }
                catch(exception e){
                    system.debug('## Error : update service work order ==> '+e.getMessage());   
                }       
            }
    }
    
    // method for creating Notes map for each work order functionality
    private void FindNotes( list<Service_Work_Order__c> SwoList){
        
        set<string> woIdSet = new set<string>(); 
        woIdSet.add('0019000001JyDJB');
        for(Service_Work_Order__c rec: SwoList){
            woIdSet.add(rec.id);
        }
        
        
        list<Note> notList = [Select Title, ParentId, IsDeleted, Id, CreatedDate, Body 
                                From Note 
                                where IsDeleted = false and ParentId in: woIdSet];
        
        
        if(notList.size()>0){
            
            for(Note rec: notList){
                if(WoNotesMap.containsKey(rec.ParentId)){
                    list<Note> tempNoteList = WoNotesMap.get(rec.ParentId);
                    tempNoteList.add(rec);
                    WoNotesMap.put(rec.ParentId, tempNoteList);
                }
                else{
                    list<Note> tempNoteList2 = new list<Note>();
                    tempNoteList2.add(rec);
                    WoNotesMap.put(rec.ParentId, tempNoteList2);
                }           
            }
        }
    }
    
    //method for creating Work Order Rooms Map for Every Work Order functionality
    private void FindWorkOrderRooms(List<Service_Work_Order__c> worderList){
        
        set<string> woIdSet = new set<string>(); 
        
        for(Service_Work_Order__c rec: worderList){
            woIdSet.add(rec.id);
        }
        
        List<Work_Order_Room__c> WORooms = [SELECT Id, Name, Work_Order__c 
                                            FROM Work_Order_Room__c 
                                            WHERE Work_Order__c in : woIdSet];
        
        
        if(WORooms.size()>0){
            for(Work_Order_Room__c woroom : WORooms){
                if(WoRoomsMap.containsKey(woroom.Work_Order__c)){
                    List<Work_Order_Room__c> tempRoomList = WoRoomsMap.get(woroom.Work_Order__c);
                    tempRoomList.add(woroom);
                    WoRoomsMap.put(woroom.Work_Order__c, tempRoomList);
                }
                else{
                    List<Work_Order_Room__c> tempRoomList2 = new List<Work_Order_Room__c>();
                    tempRoomList2.add(woroom);
                    WoRoomsMap.put(woroom.Work_Order__c, tempRoomList2);
                }           
            }
        }
    }
    
    //method for creating Work Order Rooms Map for Every Work Order functionality
    private void FindWorkOrderServices(List<Service_Work_Order__c> worderList){
        
        SET<String> woIdSet = new SET<string>(); 
        
        for(Service_Work_Order__c rec: worderList){
            woIdSet.add(rec.id);
        }
        
        List<Work_Order_Service__c> WOServices = [SELECT Id, Name, Work_Order__c 
                                                  FROM Work_Order_Service__c
                                                  WHERE Work_Order__c in : woIdSet];
        
        
        if(WOServices.size()>0){
            for(Work_Order_Service__c woservice : WOServices){
                if(WoServicesMap.containsKey(woservice.Work_Order__c)){
                    List<Work_Order_Service__c> tempServiceList = WoServicesMap.get(woservice.Work_Order__c);
                    tempServiceList.add(woservice);
                    WoServicesMap.put(woservice.Work_Order__c, tempServiceList);
                }
                else{
                    List<Work_Order_Service__c> tempServiceList2 = new List<Work_Order_Service__c>();
                    tempServiceList2.add(woservice);
                    WoServicesMap.put(woservice.Work_Order__c, tempServiceList2);
                }           
            }
        }
    }
    
    //Method for finding all Event Dates for Customer Location Functionality
    private void FindEventDates(String custId){
        List<Event> events = new List<Event>();
        allEventsDates = new Set<Date>();
        String locationId = [SELECT Id, Location__c FROM Account WHERE Id =: custId].Location__c;
        
        if(locationId != null){
            events = [SELECT Id, Subject, IsAllDayEvent, EndDateTime, StartDateTime, WhatId 
                      FROM Event
                      WHERE WhatId =: '0019000001JyDJB'
                      AND Subject = 'Call'];
            if(events.size() > 0){
                for(Event evnt : events){
                    Date startdate;
                    Date enddate;
                    if(evnt.IsAllDayEvent){
                        startdate = evnt.StartDateTime.date().addDays(1);
                        enddate = evnt.EndDateTime.date().addDays(1);
                    }else{
                        startdate = evnt.StartDateTime.date();
                        enddate = evnt.EndDateTime.date();
                    }
                    Date tempdate = startdate;
                    allEventsDates.add(startdate);
                    allEventsDates.add(enddate);
                    Integer numberOfDays = startDate.daysBetween(enddate);
                    System.debug('numberOfDaysnumberOfDays'+numberOfDays);
                    Integer i;
                    for(i=1 ; i <= numberOfDays; i++){
                        if(tempdate < enddate){
                            allEventsDates.add(tempdate);  
                        }
                        tempdate = tempdate.addDays(1);  
                    }
                }
            }
        }
    }
    
    // method for "add notes" functionality
    public void addnote(){
        if(woid != null && woid.length() > 0){
            String worderid = worderMap.get(woid).id;
            List<Service_Work_Order__c> sworder = new List<Service_Work_Order__c>();
            sworder.add(worderMap.get(woid));
            //if(sworder.get(0).Service_Date__c >= Date.Today()){
                if(worderid != null && worderid.length() > 0){
                    Note wonote = new Note();
                    wonote.ParentId = worderid;
                    wonote.Title = 'Note';
                    wonote.Body = notebody;
                    try{
                        insert wonote;  
                    }
                    catch(exception e){
                        system.debug('## Error : Add Notes for service work order ==> '+e.getMessage());
                    }
                }
            //}
        }
    }
    
    // method for "Cancel" functionality
    public void cancelworder(){
        String worderid = worderMap.get(woid).id;
        if(woid != null && woid.length() > 0){
            List<Service_Work_Order__c> sworder = new List<Service_Work_Order__c>();
            sworder.add(worderMap.get(woid));
            if(sworder != null && sworder.size() > 0){
                sworder.get(0).Cancellation_Reason__c = cancellationReason;
                sworder.get(0).Rescheduled_Reason__c = 'Request to Cancel';
                sworder.get(0).Status__c = 'Cancelled';
                try{
                    update sworder; 
                }
                catch(exception e){
                    system.debug('## Error :Cancel service work order ==> '+e.getMessage());
                }
                     
            }
        }
    }
    
    // method for "reschedule" functionality
    public void rescheduleWorder(){
        String worderid = worderMap.get(woid).id;
        if(woid != null && woid.length() > 0){
            List<Service_Work_Order__c> sworder = new List<Service_Work_Order__c>();
            Service_Work_Order__c clonedSWorder = new Service_Work_Order__c();
            sworder.add(worderMap.get(woid));
            if(sworder != null && sworder.size() > 0){
                //if(sworder.get(0).Service_Date__c >= Date.Today()){
                  
                 
                //}   
            }
        }
        rescheduleDate = null;
    }
    
    // method to get Work Order Id functionality
    public void getWorkOrderId(){
        List<Service_Work_Order__c> sworder = new List<Service_Work_Order__c>();
        System.debug('worderidworderid'+worderid);
        System.debug('wordermapwordermap'+wordermap);
        sworder.add(wordermap.get(worderid));
        if(sworder != null && sworder.size() > 0){
            WorkOrderId = sworder.get(0).Id;
        }
    }
    
    // method for "reschedule All" functionality
    public void reschedueAllWorkOrder(){
        List<Service_Work_Order__c> sworder = new List<Service_Work_Order__c>();
        date rescheduleWorkOrderServiceDate;
        
        sworder.add(worderMap.get(woid));
        
        if(sworder.size()>0){
            if(sworder[0].Service_Date__c != null)
                rescheduleWorkOrderServiceDate = sworder[0].Service_Date__c;
            
                    
            try{
                    delete sworder;
            }
            catch(exception e){
                system.debug('## rescheduling all work orders ==> '+e.getMessage());
            }
        }
        
        string customerId = apexPages.currentPage().getParameters().get('id');
        system.debug('rescheduleDate====>>>>'+rescheduleDate);
        if(rescheduleDate != null){
            
            if(rescheduleWorkOrderServiceDate == null) rescheduleWorkOrderServiceDate = rescheduleDate;
            
            list<Service_Work_Order__c> FutureworkOrderList = [SELECT Work_Team__r.Deleted__c, Work_Team__r.Captain__r.Name, Work_Team__r.Captain__c,Status__c,
                                                                    Work_Team__c, Service_Date__c, Service_Agreement__r.Customer__c, Service_Agreement__c, 
                                                                    Id, Rescheduled_Reason__c,name
                                                                 FROM Service_Work_Order__c 
                                                                 WHERE Service_Date__c >: rescheduleWorkOrderServiceDate
                                                                 AND Service_Agreement__r.Customer__c =: customerId ];
            
            if(FutureworkOrderList.size() > 0 ){
                
                try{
                    delete FutureworkOrderList;
                }
                catch(exception e){
                    system.debug('## rescheduling all work orders ==> '+e.getMessage());
                }
                
                list<Service_Agreement__c> serviceAList = [Select s.Work_Team__c
                                                            From Service_Agreement__c s
                                                            where s.Customer__c =: customerId];
                                                            
                if(serviceAList.size()>0){
                                      
                    
                  
                    try{
                         
                    }
                    catch(exception e){
                        system.debug('## rescheduling all work orders ==> '+e.getMessage());
                    }
                    
                    /*UpdateServiceWorkOrderHandler controller = new UpdateServiceWorkOrderHandler();
                    if(!Test.isRunningTest()){
                        try{
                            controller.ChangeCaptainForCustomer(New set<string>{customerId});
                        }
                        catch(exception e){
                            system.debug('## rescheduling all work orders ==> '+e.getMessage());
                        }
                    }  */ 
                }                                           
            }   
        } 
    }
    
    public void getWorkOrderNotes(){
        String worderid; 
        workOrderNotes = new List<note>();
        woid = '00001';
        if(woid != null && woid != ''){
           // worderid = worderMap.get(woid).id;
            worderid = '0019000001JyDJB';
            for(Note n:[select id,title,body,CreatedBy.Name,CreatedDate,Parent.Type 
            from Note where ParentId=:worderid Order by CreatedDate Desc,Id]){
                workOrderNotes.add(n);
            }
        }
        System.debug('workOrderNotesworkOrderNotes'+workOrderNotes);
        //return workOrderNotes;
    }
    
    public List<String> getRescheduleReasons(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Service_Work_Order__c.Rescheduled_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple)
            options.add(p.getValue());        
            
        return options;
    }
    
    public List<String> getCancelReasons(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Service_Work_Order__c.Cancellation_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple)
            options.add(p.getValue());        
            
        return options;
    }
}