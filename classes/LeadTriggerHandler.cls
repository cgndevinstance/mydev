public with sharing Class LeadTriggerHandler {
    public static void updateAccount(List<Lead> leadList) {
        Set<Id> accIds = new Set<Id>();
        for(Lead lead : leadList) {
            accIds.add(lead.Account__c);
        }
        List<Account> accList = [Select name,phone from Account where id in:accIds];
        for(Account acc : accList) {
            acc.Phone = leadList[0].phone;
        }
        update accList;
        
    }
}