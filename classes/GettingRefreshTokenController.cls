global class GettingRefreshTokenController {
    public string refreshToken{get;set;}
    public PageReference gettingcode() {
        
        String auth_url ='https://na35.salesforce.com/services/oauth2/authorize';
        String clientid = '3MVG9szVa2RxsqBZ3dfgmBzND2EH1DeEN1LCt_5_Whh0YwL0rAjzMROzY_0XLQfdzFFVngokjNzBM6b497JCK';
        String params =  
                        '?response_type=code' +
                        '&client_id=' + encodingUtil.urlencode(clientid,'UTF-8') +
                        '&redirect_uri=https://na35.salesforce.com/apex/OAuthPage' +
                        '&scope=' + encodingUtil.URLEncode('refresh_token full','UTF-8') +
                        '&state=step2';
        
        pageReference pr = New PageReference(auth_url + params);
        System.debug(pr.getContentasPdf());
        return pr; 
        
    }
    
    public pagereference gettingRefreshToken(){
       
        system.debug('1111111111111111111'+apexPages.currentPage().getParameters().get('code'));
        String clientid = '3MVG9szVa2RxsqBZ3dfgmBzND2EH1DeEN1LCt_5_Whh0YwL0rAjzMROzY_0XLQfdzFFVngokjNzBM6b497JCK';
        String clientSecret = '5830268288510489477';
        HttpRequest req = new HttpRequest();
        Http http = new Http();
    
        String auth_url = 'https://na35.salesforce.com/services/oauth2/token';
        String params =  
                            '?code=' + apexPages.currentPage().getParameters().get('code') +
                            '&grant_type=authorization_code' + 
                            '&client_id=' + encodingUtil.urlencode(clientid,'UTF-8') +
                            '&client_secret='+clientSecret + 
                            '&redirect_uri=https://na35.salesforce.com/apex/OAuthPage';
    
        req.setMethod('POST');
        req.setEndpoint(auth_url + params);
    
        HTTPResponse resp = http.send(req);
        string output = resp.getBody();
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(output);
        System.debug('mmmmmmmmmmmmmmmmmmm'+m);
        refreshToken = (String)m.get('refresh_token');
        System.debug('tokennnnnnnnnnnnnnnnnnnnnn'+refreshToken); 
       return null;
    }
   
    public void gettingTokenFromRefreshToken() {
        string refreshToken1 = '5Aep861HDR3iASSXIXtEUlB61RuFaQwo4Q6pfHxXTPiKLw9orYNGu1qHjIH.0XUc8CJcCL_2_GVkP5UlhHJMBKo';
        String clientid = '3MVG9szVa2RxsqBZ3dfgmBzND2EH1DeEN1LCt_5_Whh0YwL0rAjzMROzY_0XLQfdzFFVngokjNzBM6b497JCK';
        String clientSecret = '5830268288510489477';
        HttpRequest req = new HttpRequest();
        Http http = new Http();
    
         String auth_url = 'https://login.salesforce.com/services/oauth2/token';
        String params =  
                            '?grant_type=refresh_token' + 
                            '&refresh_token='+ refreshToken1 +
                            '&client_id=' + encodingUtil.urlencode(clientid,'UTF-8') +
                            '&client_secret='+clientSecret + 
                            '&redirect_uri=https://na35.salesforce.com/apex/OAuthPage';
    
        req.setMethod('POST');
        req.setEndpoint(auth_url + params);
    
        HTTPResponse resp = http.send(req);
        string output = resp.getBody();
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(output);
        System.debug('mmmmmmmmmmmmmmmmmmm'+m);
        String Token = (String)m.get('access_token');
        System.debug('tokennnnnnnnnnnnnnnnnnnnnn'+Token); 
        
        
        String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
        String Endpoint = 'https://na35.salesforce.com/services/data/v32.0/sobjects/contact/listviews/00B41000003qGA5/describe';
        
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(Endpoint);
        req1.setMethod('GET');
        //req.setHeader('Authorization','Bearer '+SessionId);
        req1.setHeader('Authorization', 'OAuth ' + token);
        req1.setTimeOut(120000);
        
        Http http1 = new Http();
        HTTPResponse res = http1.send(req1);
        
        System.debug('///////////////////////// json body'+res.getbody());
        String responseBody = '';
        responseBody = res.getbody();
        
        JSONParser parser = JSON.createParser(responseBody);
        While (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'query')) {
                parser.nextToken();
                if( parser.getText() != '' )
                System.debug('xxxxxxxxxxxxxxx'+parser.getText());
            }        
        } 
        
        
      
    }
  
}