/*  ClassName   :   OpportunityProductClass
    CreateBy    :   Ch.V.GopiNadh
    Date        :   27/12/2013  
    Description :   clone Opportunity and its products */

public class OpportunityProductClass
{   
   public OpportunityProductClass(Apexpages.Standardcontroller controller)
    {
    }
   /*
 *  Method name     :   cloneRec
 * Created by       :   Ch.V.Gopinadh
 * Date             :   27/12/2013
 * Description      :   Method which clone the Opportunity record and reference to the cloned record
 * Input parameters :   nill
 * Returns          :   page refernce object
 */
    public Pagereference cloneRec()
    {
        List<OpportunityLineItem> opplineitem = new List<OpportunityLineItem>();
        Opportunity opp = [SELECT ID, Name,CloseDate,StageName,TotalOpportunityQuantity FROM Opportunity WHERE Id = : apexpages.currentpage().getparameters().get('id')];
        Opportunity oppCopy = opp.clone(false,true);
        insert oppCopy;
        List<OpportunityLineItem> line = [SELECT Id,Quantity,UnitPrice,PricebookEntryId FROM OpportunityLineItem WHERE OpportunityId = : opp.Id];
        for(OpportunityLineItem c : line)
        {
            OpportunityLineItem opplineCopy = c.clone(false,true);
            opplineCopy.OpportunityId = oppCopy.Id;
            opplineitem.add(opplineCopy);
        }
        insert opplineitem;
        Pagereference p = new Pagereference('https://ap1.salesforce.com/'+oppCopy.Id);
        return p;
    }
}