public with sharing class Appraisal {
    
    public Decimal Calculate{get;set;}
    
    public Decimal timesubdel{get;set;}
    public Decimal totdel{get;set;}
    
    public Decimal TotBillable{get;set;}
    public Decimal TotHours{get;set;}
    
    public Decimal CompProjects{get;set;}
    public Decimal AssignedProjects{get;set;}
    
    public Decimal tsintime{get;set;}
    public Decimal totts{get;set;}
    
    public Appraisal__c app{get;set;}
    private Apexpages.StandardController stdcontroller;
    
    public Appraisal(ApexPages.StandardController controller) {
        this.stdcontroller = controller;
        app = ((Appraisal__c)controller.getRecord());
        
    }
    public PageReference GenerateDelivrableTime(){
        timesubdel = app.Timely_Submission_of_deliverables__c;
        totdel = app.Number_of_total_deliverables__c;
        app.Target__c = 100;
        app.Weightage__c = 30;
        if(totdel!=0 & totdel!=null && timesubdel!=null)
            Calculate = ((timesubdel/totdel)*app.Target__c).setscale(2);
        if(Calculate !=null)
            app.Appraisee_score__c = ((Calculate/app.Target__c)*app.Weightage__c).setscale(2);
        return null; 
    }
    
    public PageReference GenerateBillableTime(){
        TotBillable = app.Total_Billable_Hours__c;
        TotHours = app.Total_Available_Hours__c;
        app.Billable_Weightage__c = 20;
        app.Billable_Target__c = 60;
        if(TotHours!=0 & TotHours!=null && TotBillable!=null)
            Calculate = ((TotBillable/TotHours)*100);
        if(Calculate !=null)
            app.Appraisee_Billable_Score__c = ((Calculate/app.Billable_Target__c)*app.Billable_Weightage__c).setscale(5);
        System.debug('bbbbbbbbbbbbb'+app.Appraisee_Billable_Score__c);
        return null; 
    }
    
    public PageReference GenerateProductScore(){
        CompProjects = app.No_Of_Projects_Completed__c;
        AssignedProjects = app.Projects_Assigned__c;
        app.Prod_Weightage__c = 20;
        app.Prod_Target__c = 100;
        if(AssignedProjects!=0 & AssignedProjects!=null && CompProjects!=null)
            Calculate = ((CompProjects/AssignedProjects )*100);
        if(Calculate !=null)
            app.Appraisee_Prod_Score__c = ((Calculate/app.Prod_Target__c)*app.Prod_Weightage__c).setscale(2);
        return null; 
    }
    
    public PageReference GenerateTimesheetScore(){
        tsintime = app.No_of_times_Ts_Submitted_on_time__c;
        totts = app.Tot_No_of_Ts_Submitted__c;
        app.Timesheet_Weightage__c= 15;
        app.Timesheet_Target__c = 100;
        if(totts!=0 & totts!=null && tsintime!=null)
            Calculate = ((tsintime/totts )*100);
        if(Calculate !=null)
            app.Appraisee_timesheet_score__c = ((Calculate/app.Timesheet_Target__c)*app.Timesheet_Weightage__c).setscale(2);
        return null; 
    }
    public Pagereference next(){
        //this.stdcontroller.save();
        //Pagereference p = page.Competencies;
        //p.setRedirect(true); 
        return page.Competencies;
        
    }
    public Pagereference previous(){
        return page.Appraisal_Form;
        
    }
    public PageReference Others(){
        return page.others;
    }
    public PageReference PrevComp(){
        return page.Competencies;
    }
    
    public PageReference SaveAppraisal(){
        this.stdcontroller.save();
        return null;
    }

}