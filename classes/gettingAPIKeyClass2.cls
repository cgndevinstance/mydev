Global class gettingAPIKeyClass2 implements Schedulable {
   
    Public static String outxmlstring;
    Public static String xmlstring;
    Public static string finalvalue;
    Public static Integer pos1;
    Public static Integer pos2;
    Public static string[] xmlToString;
    
    global void execute(SchedulableContext ctx) {
       getContent();
    }  
    @future(callout=true)   
    Public static void getContent()  {
        
        string email = 'syed@mysalesforcecrm.com';
        string password = 'Tvarana@1234';
        string user_id = '45fdaac4a9338e31032862eebebfa8e0';
        string endpoint = 'https://pi.pardot.com/api/login/version/3?email='+email+'&password='+password+'&user_key='+user_id ;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        Http h = new Http();
        HttpResponse res = h.send(req);
        
        Dom.Document doc = res.getBodyDocument();
        Dom.XMLNode address = doc.getRootElement();
        String api_key = address.getChildElement('api_key', null).getText();
        gettingAllProspects(api_key);
        
    }
    
    
    Public static void gettingAllProspects(string api_key){
        List<Lead> allProspects = new List<Lead>();
        List<CampaignMember> allCampaignMembers = new List<CampaignMember>();
        string user_id = '45fdaac4a9338e31032862eebebfa8e0';
        string endpoint = 'https://pi.pardot.com/api/prospect/version/3/do/query?user_key='+user_id+'&api_key='+api_key;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        Http h = new Http();
        HttpResponse res = h.send(req);
        xmlstring = res.getBody();
        xmlToString = xmlstring.split('<prospect>');
        for(integer i=1; i<xmlToString.size();i++){  
            xmlToString[i] = 'aaaa'+xmlToString[i];
            Lead newLead = new Lead();

            newLead.salutation = gettingValuesFromXML('<salutation>','</salutation>',xmlToString[i]);
            newLead.firstName = gettingValuesFromXML('<first_name>','</first_name>',xmlToString[i]);
            newLead.lastName = gettingValuesFromXML('<last_name>','</last_name>',xmlToString[i]);
            //newLead.email = gettingValuesFromXML('<email>','</email>',xmlToString[i]);
            //newLead.Status='Working - Contacted';
            newLead.company= 'aaaaaa';
            //newLead.Company = gettingValuesFromXML('<company>','</company>',xmlToString[i]);
            newLead.Website = gettingValuesFromXML('<website>','</website>',xmlToString[i]);
            newLead.Title = gettingValuesFromXML('<job_title>','</job_title>',xmlToString[i]);
            newLead.City = gettingValuesFromXML('<city>','</city>',xmlToString[i]);
            newLead.Street = gettingValuesFromXML('<address_one>','</address_one>',xmlToString[i]);
            newLead.Country = gettingValuesFromXML('<country>','</country>',xmlToString[i]);
            newLead.State = gettingValuesFromXML('<state>','</state>',xmlToString[i]);
            newLead.PostalCode = gettingValuesFromXML('<zip>','</zip>',xmlToString[i]);
            newLead.Phone = gettingValuesFromXML('<phone>','</phone>',xmlToString[i]);
            newLead.Fax = gettingValuesFromXML('<fax>','</fax>',xmlToString[i]);
            newLead.LeadSource = gettingValuesFromXML('<source>','</source>',xmlToString[i]);
            //newLead.AnnualRevenue = Decimal.valueOf(gettingValuesFromXML('<annual_revenue>','</annual_revenue>',xmlToString[i]));
            //newLead.NumberOfEmployees = Integer.valueOf(gettingValuesFromXML('<employees>','</employees>',xmlToString[i]));
            newLead.Industry = gettingValuesFromXML('<industry>','</industry>',xmlToString[i]);
            newLead.Description = gettingValuesFromXML('<comments>','</comments>',xmlToString[i]);
            
            allProspects.add(newLead);
        }
        if( allProspects.size()> 0 ) {
            
            insert allProspects;
            
         /*   for(lead l:allProspects){
                CampaignMember campaignmemberToAdd = new CampaignMember();
                campaignmemberToAdd.CampaignId =  '70190000000gB3K';
                campaignmemberToAdd.leadid =  l.id ;
                allCampaignMembers.add(campaignmemberToAdd);
           }
           insert allCampaignMembers;*/
        }
        
    }
    
    
    Public static string gettingValuesFromXML(string startNode,string endNode,string mainstring){
        
        pos1 = mainstring.indexOf(startNode);
        if(pos1==-1)
            return '';
        pos2 = mainstring.indexOf(endNode);
        finalValue = mainstring.substring(pos1+startNode.length(),pos2);
        return finalValue;
        
   }
   
    
}