public class FullCalndarController2{
    public string EventBody{get;set;}
    public string ResourceBody{get;set;}
    public string EventId{get;set;}
    public string sdate{get;set;}
    public string edate{get;set;}
    public string ResId{get;set;}
    
    public Map<string,Task> EventMap = new Map<string,Task>();
   public FullCalndarController2() {
        FetchTaskEvents();
    }
    public void FetchTaskEvents () {
        EventBody = ' ';
        ResourceBody = '';
        Map<String,string> EventUsersMap = new Map<String,string>();
        List<Task> EventList = [SELECT Id,OwnerId,Subject,Status,Owner.Name, ActivityDate ,Start_Date__c,End_Date__c from Task where owner.isActive = true and Start_Date__c!= null and End_Date__c!=null ];
        System.debug('EventListEventList'+EventList);
        for ( Task evnt : EventList ) {
             if ( evnt.ActivityDate != null ) {
                System.debug('111111111111'+evnt.Start_Date__c);
                string s1 = string.valueOf(evnt.Start_Date__c);
                string s2 = string.valueOf(evnt.End_Date__c);
                DateTime sdt = DateTime.valueofGmt(s1);
                DateTime edt = DateTime.valueofGmt(s2);
                edt = edt.addHours(1);
                System.debug('2222222222222'+sdt);
                System.debug('333333333333'+edt);
                 EventMap.put(evnt.id,evnt);
                 EventBody += '{';
                 EventBody += 'title: \''+evnt.Subject+'\'  , ';
                 EventBody += 'start: \''+sdt+'\'  , ';
                 EventBody += 'end: \''+edt+'\'  , ';
                 EventBody += 'resourceId: \''+evnt.OwnerId+'\'  , ';
                 EventBody += 'id: \''+evnt.id+'\'';
                 EventBody += '},';
                 
                 ResourceBody += '{';
                 ResourceBody += 'id: \''+evnt.OwnerId+'\'  , ';
                 ResourceBody += 'eventColor:\''+'red'+'\'  , ';
                 ResourceBody += 'title: \''+evnt.Owner.Name+'\'';
                 ResourceBody += '},';
             }
         }
         EventBody = EventBody.substringBeforeLast(',');
         ResourceBody = ResourceBody.substringBeforeLast(',');
         system.debug('EventBodyEventBody'+EventBody);
          system.debug('ResourceBody'+ResourceBody);
         
    }
    public void updateEvent(){
        System.debug('sssssssssssss'+sdate);
        system.debug('eeeeeeeeeeee'+edate);
        List<String> StDate = sdate.split('-');
        List<String> EnDate = edate.split('-');
        Integer shour = Integer.valueOf(StDate[3]) - 5;
        Integer ehour = Integer.valueOf(enDate[3]) - 5;
        ehour = ehour - 1;
        if ( shour < 0 ) {
            shour = shour + 24;
        }
        if ( ehour < 0 ) {
            ehour = ehour + 24;
        }
        Integer smins = Integer.valueOf(StDate[4]) - 30;
        Integer emins = Integer.valueOf(enDate[4]) - 30;
        if ( smins < 0 ) {
            smins = smins + 60;
            shour = shour - 1;
        }
        if ( emins < 0 ) {
            emins = emins + 60;
            ehour = ehour - 1;
        }
        System.debug('11111111111111'+shour+'----'+smins);
       Task event = EventMap.get(EventId);
       event.Start_Date__c = DateTime.valueOf(StDate[0]+'-'+StDate[1]+'-'+StDate[2]+' '+shour+':'+smins+':00');
       event.End_Date__c = DateTime.valueOf(EnDate[0]+'-'+enDate[1]+'-'+enDate[2]+' '+ehour+':'+emins+':00');
       //event.Start_Date__c = DateTime.valueOf('2015-07-05 12:30:00');
       //event.End_Date__c = DateTime.valueOf('2015-07-05 13:30:00');
        
        event.OwnerId = Resid; 
        update event;
    }
}