public with sharing class jqmMyEvents {
    public List<event> eventList{get;set;}
    public List<event> dayEvtList{get;set;}
    public String selDate{get;set;}
    public Boolean eventsForTheDay{get;set;}
    
    public String eventId{get;set;}
    public Event evt{get;set;}
    public List<Account> Acct{get;set;}
    public String latitude{get;set;}
    public String longitude{get;set;}
    public String queryStr{get; set;}
    public String userLocale{get;set;}
    
    
    public jqmMyEvents(){
        eventList = [select Id, ActivityDate from event where OwnerId =: Userinfo.getUserId()];
        userLocale = UserInfo.getLocale();
        //dayEvent();
    }
    
    public void dayEvent(){
        selDate = Apexpages.currentPage().getParameters().get('selDate');
        system.debug('***** '+selDate+' UserInfo.getLocale(): '+UserInfo.getLocale()+' '+date.today());
        List<String> newDateString = selDate.split('/');
        Integer month = Integer.valueOf(newDateString[0]);
        Integer day = Integer.valueOf(newDateString[1]);
        Integer year = Integer.valueOf(newDateString[2]);
        
        date myDate = date.newInstance(year, month, day);
        
        dayEvtList = [select Id, subject, Who.Name, Location, StartDateTime, EndDateTime, Description, ActivityDate from event where ActivityDate =: myDate and OwnerId =: Userinfo.getUserId()];
        if(dayEvtList.size() == 0){
            eventsForTheDay = false;
        }else{
            eventsForTheDay = true;
        }
    }
    
    
    
    
    public void eventDetails(){
        try{
            eventId = ApexPages.currentPage().getParameters().get('eventId');
            evt = [select Id, subject, Who.Name, Location, StartDateTime, EndDateTime from Event where Id =: eventId];
        }catch(Exception e){}
    }
     public void mDtls(){
        try{
            latitude = ApexPages.currentPage().getParameters().get('latitude');
            longitude = ApexPages.currentPage().getParameters().get('longitude');
            system.debug('********'+latitude+longitude+'********');
            queryStr = 'SELECT Id, Name, Location__latitude__s, Location__longitude__s, BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity FROM Account WHERE DISTANCE(Location__c, GEOLOCATION('+latitude+', '+longitude+'), \'km\') < 15';
            Acct = Database.query(queryStr);
            system.debug('************'+Acct);
        }catch(Exception e){}
    }
}