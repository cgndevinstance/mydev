public class multiselect {

   

       
     Id aid = apexpages.currentPage().getParameters().get('a1'); 
  
    List<selectOption> options = new List<selectOption>();  
           
    Public List<string> leftselected{get;set;}
    public Contact contact1{get;set;}
       Public List<string> rightselected{get;set;}

    Set<string> leftvalues = new Set<string>();

    Set<string> rightvalues = new Set<string>();
    private apexpages.standardcontroller con;
    public multiselect(ApexPages.StandardController controller) {
        leftselected = new List<String>();

        rightselected = new List<String>();
        con = controller;
        System.debug(contact1);
        
         
          
        }
       
    
     public void test(){
     
        for(opportunity o : [ select id, name from opportunity where accountid = : aid]){
          leftvalues.add(o.name); 
     }
     }

   

        public PageReference selectclick(){

        rightselected.clear();

        for(String s : leftselected){

            leftvalues.remove(s);

            rightvalues.add(s);

        }

        return null;

    }
    
    public List<selectOption> getacc1() {
        

        options.add(new selectOption('', '- None -')); 

        for (Account account : [SELECT Id, Name FROM Account]) { 

            options.add(new selectOption(account.id, account.Name)); 

        }
        return options; 
    }

     

    public PageReference unselectclick(){

        leftselected.clear();

        for(String s : rightselected){

            rightvalues.remove(s);

            leftvalues.add(s);

        }

        return null;

    }
    public PageReference save1(){
    
        con.save();
        return null;
    }


    public List<SelectOption> getunSelectedValues(){

        List<SelectOption> options = new List<SelectOption>();

        List<string> tempList = new List<String>();

        tempList.addAll(leftvalues);

        tempList.sort();

        for(string s : tempList)

            options.add(new SelectOption(s,s));

        return options;

    }
   

    public List<SelectOption> getSelectedValues(){

        List<SelectOption> options1 = new List<SelectOption>();

        List<string> tempList = new List<String>();

        tempList.addAll(rightvalues);

        tempList.sort();

        for(String s : tempList)

            options1.add(new SelectOption(s,s));

        return options1;

    }

}