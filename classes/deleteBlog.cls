public class deleteBlog
{
    public boolean checked{get;set;}
    public List<Member__c> memTmpLst;
    public List<mem> memList {get;set;}
    public Member__c memb;
    String[] selectedNam = new String[]{};
    List<String> selectednames =new List<String>();
    public List<String> temp {get;set;}
    public List<Member__c> memDelList;  
    public Integer i; 
   
    public class mem
    {
      public Member__c themem {get; set;}
      public Boolean checked{get; set;}
      
      public mem(Member__c m,Boolean s)
      {
        themem = m;
        checked = s;
      }
    }

    public deleteBlog(ApexPages.StandardController controller)
    {
      this.memb = (Member__c)controller.getRecord();
    }
   
    public void find()
    {
      String nam = memb.Name;
      memList = new List<mem>();
      if(memList == null)
      {
        memList = new List<mem>();
      }  
      String sql = 'SELECT Name,Mobile_Number__c,E_Mail_Id__c FROM Member__c WHERE Name LIKE\'%'+nam+'%\'';
      memTmpLst = Database.query(sql);
      for(Member__c m:memTmpLst)
      {
        mem me = new mem(m,false);
        memList.add(me);
      }    
    }   
   
    public void del()
    {
      memDelList = new List<Member__c>();
      for(mem m:memList)
      {
        if(m.checked == true)
        {
          if(memDelList==null)
          {
            memDelList = new List<Member__c>();
          }
          memDelList.add(m.themem);     
     
        }             
      }
      Delete memDelList;
      find();
    }
}