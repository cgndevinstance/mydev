public class Web2LeadInputFile {
    String email;
    String LastName;
    String Company;
    String myfile;
    String myfile1;
    public void addFile() {
        Map<String, String> l = ApexPages.currentPage().getParameters();
         email = l.get('email');
        LastName = l.get('last_Name');
        Company = l.get('Company');
        myfile = l.get('choose_file');
        myfile1 = l.get('cfile');
        List<Lead> le = [SELECT Id, Email, sameEmails__c FROM LEAD WHERE Email =: email LIMIT 1];
        if(le.size() > 0) {
            List<Lead> leadLst = new List<Lead>();
            for(Lead leadsToUpdate : le) {
                if(leadsToUpdate.sameEmails__c == null) {
                    leadsToUpdate.sameEmails__c = 0;
                }
                leadsToUpdate.sameEmails__c = leadsToUpdate.sameEmails__c + 1;
                leadLst.add(leadsToUpdate);
            }
            update leadLst;            
        }
        else { 
            Lead l2 = new Lead();
            l2.LastName = LastName;
            l2.Email = email;
            l2.Company = Company;
            insert l2;
            
            Attachment att = new Attachment();
            att.ParentId = l2.Id;
            att.Name = myfile;
            att.Body = Blob.valueOf(myfile1);
            insert att;
        }
    }
}