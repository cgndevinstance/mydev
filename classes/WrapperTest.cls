public class WrapperTest {

    public Question__c qobj{get;set;}
    public List<Question__c> questions{get; set;}
    public List<Question__c> questions1{get; set;}
    public Question__c theLuckyUser {get; set;}
    public List<Double> sameQuestion;
    public Double same = -1;
    public Set<Question__c> noDuplicate {get; set;}
    public List<Question__c> display { get; set;}
    public List<Question__c> finalQuestions {get; set;}
    public List<String> values;
    public Integer marks {get;set;}    
    public List<WrapperClassEx> test = new List<WrapperClassEx>(); 
    
    public questions_size__c qsize{get;set;}
    public Registration__c register {get;set;}
    
    public String uname{get;set;}
    public String pwd{get;set;} 
          
    public String s{get;set;}
    public String addcolor{get;set;}
    public String addcolor1{get;set;}
    public String addcolor2{get;set;}
    public String addcolor3{get;set;}
    public String addcolor4{get;set;}
    public String addcolor5{get;set;}
    public String addcolor6{get;set;}
    public String addcolor7{get;set;}
    public String addcolor8{get;set;}
    public String addcolor9{get;set;}
    public String addcolor10{get;set;}
    
    public String displaycolor1{get;set;}
    public String displaycolor2{get;set;}
    public String displaycolor3{get;set;}
    public String resultpage{get;set;}
    
    public List<Login__c> log = new List<Login__c>();
    public List<WrapperClassEx> wrapList{get;set;}
    
    public String answer ='';
   
   
    public Integer CounterIndex {get; set;}    
    
    public WrapperTest() {
        marks = 0;
        CounterIndex = 0;
        getWrapperObj();
        displaycolor1='visible';
        displaycolor2='visible1';
        displaycolor3='nocolor';
        resultpage='addcss';
        
    }
   
    public PageReference next() {
      WrapperClassEx w = wraplist[counterindex];
      if(w.answer==null){
          ApexPages.message msg = new ApexPages.message(ApexPages.severity.ERROR,'Please select an option');
          Apexpages.addMessage(msg);
      }
      else{
            CounterIndex++;
            
            
        if(w.selected!=true){
           if(counterindex==1) 
               addcolor1='visible';
           if(counterindex==2)
               addcolor2='visible';
           if(counterindex==3)
               addcolor3='visible';
           if(counterindex==4) 
               addcolor4='visible';
           if(counterindex==5)
               addcolor5='visible';
           if(counterindex==6)
               addcolor6='visible';
           if(counterindex==7) 
               addcolor7='visible';
           if(counterindex==8)
               addcolor8='visible';
           if(counterindex==9)
               addcolor9='visible';
           if(counterindex==10)
               addcolor10='visible';
         }
         else{
           if(counterindex==1) 
               addcolor1='visible1';
           if(counterindex==2)
               addcolor2='visible1';
           if(counterindex==3)
               addcolor3='visible1';
           if(counterindex==4) 
               addcolor4='visible1';
           if(counterindex==5)
               addcolor5='visible1';
           if(counterindex==6)
               addcolor6='visible1';
           if(counterindex==7) 
               addcolor7='visible1';
           if(counterindex==8)
               addcolor8='visible1';
           if(counterindex==9)
               addcolor9='visible1';
           if(counterindex==10)
               addcolor10='visible1';
         }
         if(counterIndex >= wraplist.size()){
               CounterIndex = wraplist.size() - 1;
         }
          
       }
     
      return null;
   }
   
   public void skip() {
       CounterIndex++;
       if(counterIndex >= wraplist.size()){
           CounterIndex = wraplist.size() - 1;
       }
       
       WrapperClassEx w = wraplist[counterindex-1];
       w.answer='';
       wraplist[counterindex-1].selected=false;
       
           if(counterindex==1) 
               addcolor1='hidden';
           if(counterindex==2)
               addcolor2='hidden';
           if(counterindex==3)
               addcolor3='hidden';
           if(counterindex==4) 
               addcolor4='hidden';
           if(counterindex==5)
               addcolor5='hidden';
           if(counterindex==6)
               addcolor6='hidden';
           if(counterindex==7) 
               addcolor7='hidden';
           if(counterindex==8)
               addcolor8='hidden';
           if(counterindex==9)
               addcolor9='hidden';
          if(counterindex==10)
               addcolor10='hidden';
   }
   public void reset(){
       counterindex++;
       WrapperClassEx w = wraplist[counterindex-1];
       w.answer='';
       counterindex--;
   }
    
    
    public void prev() {
        WrapperClassEx w = wraplist[counterindex];
        System.debug('111111111'+w.answer);
        system.debug('222222222'+w.qobj);
        CounterIndex--;
        
    }
    
     public String getAnswer(){
         return answer;
     }
     public void setAnswer(String Answer){
         this.answer = answer;
     }
        public List<WrapperClassEx> getWrapperObj() {
        qsize = questions_size__c.getall().values();
        System.debug('aaaaaaaaaaaa'+qsize.QsSize__c);
        wrapList = new List<WrapperClassEx>();
        questions = [SELECT Name, Question__c,Directions_To_Solve__c, Option1__c, Option2__c, Option3__c, Option4__c, Option5__c FROM Question__c ];
        questions1 = new List<Question__c>();
        for(Integer i=0; i< questions.size();i++) {
            sameQuestion = new List<Double>();
            sameQuestion.add(same);
            Double theChosenOne = Math.round(math.random()*(questions.size()-1));
            theLuckyUser = questions[theChosenOne.intValue()];
            questions1.add(theLuckyUser);
        }
        noDuplicate = new Set<Question__c>();
        for(Question__c q : questions1) {
            noDuplicate.add(q);
        }

        display = new List<Question__c>();
        for(Question__c q1 : noDuplicate) {
            display.add(q1);
        }
          
        finalQuestions = new List<Question__c>();  
        for(Integer i=0; i<qsize.QsSize__c; i++) {
            finalQuestions.add(display[i]);
            
        }
        for(Question__c q : finalQuestions){
            WrapperClassEx we = new WrapperClassEx(q,'');
            wrapList.add(we);
        }
         for(WrapperClassEx w : wrapList){
          test.add(w);
         }   
          
        return wraplist;
    }
    public PageReference login(){
         Integer i = 0;
         log = [select Register_Id__c , password__c from Login__c];
        
        if(log.size()>0){
            for(Login__c l : log){
                if(l.Register_Id__c == uname && l.password__c == pwd){
                    i=1;
                    break;
                }    
                      
            }
        
        } 
        if(i==1)
          return page.Instructionspage;
        else{
            ApexPages.message msg = new ApexPages.message(ApexPages.severity.ERROR,'Invalid Username Or Password');
          Apexpages.addMessage(msg);
          return null;
        }
            
        
        
    }
    
    public PageReference start(){
         
       return page.TestWrapper;
    }
    public PageReference starttechncial(){
         PageReference ref = page.technicalquestions;
         ref.getParameters().put('uname',uname);
         ref.getParameters().put('pwd',pwd);
         ref.setRedirect(true);
        return ref;
    }
    
    public PageReference submit(){
              
           
               List<string> selectedans = new List<String>(); 
               List<string> dbans = new List<String>(); 
          
             for(WrapperClassEx w : wraplist){
                  selectedans.add(w.answer);
                  
             }       
             for(WrapperClassEx w : test){        
                 Question__c q = w.qobj;
                 Answer__C a = [select Answer__c from Answer__c where Question__c  =: q.id];  
                 dbans.add(a.answer__c);                
                                    
            }
            
            for(Integer i = 0 ; i<qsize.QsSize__c ; i++){
            
               if(selectedans[i] == dbans[i]){
                              
                    marks = marks + 1;
               }   
            }                           
           Login__c lo = [select Marks_Scored_in_Aptitude__c from Login__c where Register_Id__c =: uname and password__c =:pwd];
           lo.Marks_Scored_in_Aptitude__c = marks;
           update lo;                           
           return page.Result;
    }
    public class WrapperClassEx {
        public Question__c qObj {get; set;}
        public Boolean selected{get;set;}
        public String answers {set;}
        public String answer {get; set;}
        public boolean checked{get;set;}
        public String selected2{get; set;}
        
        public WrapperClassEx(Question__c q,String s) {
            this.qObj = q;
            this.answer = s; 
            selected=false;
            checked=true;
            
        }
     
        
        List<SelectOption> options = new List<SelectOption>();
        public List<SelectOption> getAnswers() {
            options.clear();
            options.add(new SelectOption('A',qobj.option1__c));
            options.add(new SelectOption('B',qobj.option2__c));
            if(qobj.option3__c!=null) 
            options.add(new SelectOption('C',qobj.option3__c));
            if(qobj.option4__c!=null)
            options.add(new SelectOption('D',qobj.option4__c));
            if(qobj.option5__c!=null)
            options.add(new SelectOption('E',qobj.option5__c));
            return options; 
            
       }
    }

}