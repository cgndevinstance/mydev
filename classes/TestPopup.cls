public with sharing class TestPopup {

    public Boolean displayPopup {get;set;}
    public String mail{get;set;}
    public Contact c{get;set;}
    public TestPopup(){
        c=[select id,email from Contact where id=:ApexPages.currentPage().getParameters().get('id')];

    }
       
    public void showPopup()
    {
        
    displayPopup = true;

    
    }
    
    public void closePopup() {
        displayPopup = false;
        
    }
    
    public void save1()
    {
        System.debug('111111111'+mail);
        displayPopup = false;
        c.email=mail;
        update c;
        
        /*PageReference p=new Pagereference('https://c.ap1.visual.force.com/apex/popuppage?id='+0039000000);
        p.setRedirect(true);
        return p;*/
        
        
    }
    


}