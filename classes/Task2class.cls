public class Task2class{
     account a;
     public List<mycontact> clist{get;set;}
     public list<contact> cont{get;set;}
        public Task2class(ApexPages.StandardController controller) {
           a=[select id from account where id=:apexpages.currentpage().getParameters().get('id')];
           cont=[select id,name,Email,phone from contact where AccountId=:a.id];
           
       
        if(clist==null){
            clist = new List<mycontact>();
            for(contact c:cont){
                
                clist.add(new mycontact(c));
            }
            
            
        }
        
    }
    
    public pagereference deleteRow(){
        
        List<contact> selectedlist=new List<contact>();
        for(mycontact mycon:clist){
            
            if(mycon.selected==true){
                selectedlist.add(mycon.con);
                
            }
            
        }
        for(contact con:selectedlist){
            delete con;
        }
        return null;
    }
    public class mycontact{
        
        public contact con{get;set;}
        public Boolean selected{get;set;}
        public mycontact(contact c){
            selected=false;
            con=c;
        }
       
    }
}