global class ExpireNotify implements Database.Batchable<Contact>
{
    global List<Contact> start(Database.BatchableContext bc)
    {
        Date d = Date.today(); 
        System.debug(d); 
        Contact[] soql = [SELECT Expire_Date__c, Name, Email FROM Contact WHERE Expire_Date__c =: d];     
        return soql;
    }
   
    global void execute(Database.BatchableContext bc, List<Contact> recs)
    {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        for(Contact m : recs)
        {
            List<String> toAddresses = new List<String>();           
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            toAddresses.add(m.Email);
            mail.setToAddresses(toAddresses);
            mail.setSubject('Your Contact Got Expired');
            String messageBody = '<html><body>Hi ' + m.Name + ',<br>Your account Expires today. <br>Kindly contact your administrator.<br><br><b>Regards,</b><br>Ch.V.Gopinadh</body></html>';
            mail.setHtmlBody(messageBody); 
            mailList.add(mail);          
        } 
        Messaging.sendEmail(mailList);
    }
   
    global void finish(Database.BatchableContext bc)
    {
    }
}