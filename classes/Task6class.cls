/*  ClassName   :   Task6class
    CreateBy    :   Ch.V.GopiNadh
    Date        :   17/12/2013  
    Description :   clone account and its contacts */

public class Task6class
{   
   public Task6class(Apexpages.Standardcontroller controller)
    {
    }
   /*
 *  Method name     :   cloneRec
 * Created by       :   Ch.V.Gopinadh
 * Date             :   17/12/2013
 * Description      :   Method which clone the account record and reference to the cloned record
 * Input parameters :   nill
 * Returns          :   page refernce object
 */
    public Pagereference cloneRec()
    {
        List<Contact> cons = new List<Contact>();
        List<Estimate__c> esms = new List<Estimate__c>();
        Account acc = [SELECT ID, Name,type,industry,AnnualRevenue,phone,rating FROM Account WHERE Id = : apexpages.currentpage().getparameters().get('id')];
        Account accCopy = acc.clone(false,true);
        insert accCopy;
        List<Contact> con = [SELECT Id, LastName, AccountId FROM Contact WHERE AccountId = : acc.Id];
        for(Contact c : con)
        {
            Contact conCopy = c.clone(false,true);
            conCopy.AccountId = accCopy.Id;
            cons.add(conCopy);
        }
        insert cons;
        List<Estimate__c> es = [SELECT Id, AccountName__c,Company_name__c FROM Estimate__c WHERE AccountName__c = : acc.Id];
        for(Estimate__c c : es)
        {
           Estimate__c conCopy = c.clone(false,true);
            conCopy.AccountName__c = accCopy.Id;
            
            esms.add(conCopy);
        }
        insert esms;
        Pagereference p = new Pagereference('https://ap1.salesforce.com/'+accCopy.Id);
        return p;
    }
}