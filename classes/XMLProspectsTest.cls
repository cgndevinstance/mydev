@isTest
global class XMLProspectsTest implements HttpCalloutMock {
     
     global HTTPResponse respond(HTTPRequest req) {
        
        System.assertEquals('https://pi.pardot.com/api/prospect/version/3/do/query?user_key=45fdaac4a9338e31032862eebebfa8e0&api_key=9dfef011ef18711ddb5525737c006ccb&offset=0',req.getEndpoint());

        System.assertEquals('POST', req.getMethod());
        
        HttpResponse res = new HttpResponse();
        

        res.setBody('<?xml version="1.0" encoding="UTF-8"?><rsp stat="ok" version="1.0"><result><total_results>2895</total_results><prospect><id>314368</id><salutation></salutation><email>maisa.abusoud@astrazeneca.com</email></prospect>'); 
        res.setStatusCode(200);
        return res;
    }
}