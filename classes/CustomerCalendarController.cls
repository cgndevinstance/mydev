public with sharing class CustomerCalendarController {
    public String todayDate{get;set;}
    public list<integer> dateCalendarOne{get;set;}
    public string CustomerId{get;set;}
    public String LocationId{get;set;}
    public Boolean ischeck{get;set;}
    private date presentFirstDate{get;set;}
    
    public CustomerCalendarController(apexPages.standardController sc){
        ischeck = true;
        CustomerId = sc.getId();
        String locId = [SELECT Id, Location__c FROM Account WHERE Id =: CustomerId].Location__c;
        if(locId != null){
            LocationId= locId;
        }
        presentFirstDate = system.today();
        changeDate(presentFirstDate);
        Date dte = Date.Today(); 
        
        List<String> monthlist = new List<String>();
        monthlist.add('January');
        monthlist.add('February');
        monthlist.add('March');
        monthlist.add('April');
        monthlist.add('May');
        monthlist.add('June');
        monthlist.add('July');
        monthlist.add('August');
        monthlist.add('September');
        monthlist.add('October');
        monthlist.add('November');
        monthlist.add('December');
        
        Integer month = dte.month();
        todayDate = monthlist.get(month-1) +' '+ dte.day()+', ' + dte.year();
 
        System.debug('=====>>>>>'+ presentFirstDate);
    }
    
    private void changeDate(date selectedDate){
        
        dateCalendarOne = new list<integer>();
        
        dateCalendarOne.add(selectedDate.day());
        dateCalendarOne.add(selectedDate.month());
        dateCalendarOne.add(selectedDate.year());
    }
    
    
    public void moveBackward(){
        presentFirstDate = presentFirstDate.addmonths(-1);
        System.debug('=====>>>>>'+ presentFirstDate);
        changeDate(presentFirstDate);
    } 
    
    public void moveForward(){
        presentFirstDate = presentFirstDate.addmonths(1);
        System.debug('=====>>>>>'+ presentFirstDate);
        changeDate(presentFirstDate);
    }
}