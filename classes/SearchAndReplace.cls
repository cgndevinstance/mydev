global class SearchAndReplace implements Database.Batchable<sObject>,database.stateful{
   
   staticExample ex2 = null;
   global Database.QueryLocator start(Database.BatchableContext BC){
      ex2 = new staticExample();
      ex2.method1();
      ex2.method2();
      string query = 'select id from account limit 1';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
    
     ex2.method2();
   }
   global void finish(Database.BatchableContext BC){
   
   }
}