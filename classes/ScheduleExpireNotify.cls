global class ScheduleExpireNotify implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        ExpireNotify en = new ExpireNotify();
        Database.executeBatch(en);
    }
}