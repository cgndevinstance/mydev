public with sharing class PaginationUsingWrapperClass {


  
    public PaginationUsingWrapperClass()
    {
        
    }
  public apexpages.standardsetcontroller con
    {
        get
        {
            if(con == Null)
            {
                con = new ApexPages.standardsetcontroller(Database.getQueryLocator([Select Id, Name, Phone, Email from Contact Order By LastName limit 100]));
                con.setpagesize(1);
            }
            return con;
        }private set;
    }
    
    
    public list<Contact> lst = new list<Contact>();
    
   
    public list<Contact> getlistvalues()
    {
         for( Contact ct:(list<Contact>)con.getrecords())
         {
             lst.add(ct);
         }
       
        return lst;
    }
        
    /*public class wrapper
    {
        public String Id{set;get;}
        public String Name{set;get;}
        public String email{set;get;}
        public String Phone{set;get;}
    }*/
    public void next()
    {
        con.next();
    }
    public void previous()
    {
        con.previous();
    }
    public void first()
    {
        con.first();
    }
    public void last()
    {
        con.last();
    }
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
}