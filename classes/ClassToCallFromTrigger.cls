public with sharing class ClassToCallFromTrigger {
	public ClassToCallFromTrigger(){
		System.debug('This is Constructor');
	}
	public Integer Method1(Integer a, Integer b){
		System.debug('this is method1');
		Integer c = a + b;
		return c;
	}
}