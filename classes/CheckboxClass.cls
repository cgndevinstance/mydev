public with sharing class CheckboxClass {

 public String m_LastName{get;set;}
    
    
    public Contact objContact{get;set;}
    public Boolean m_Display{get;set;}
    public Boolean m_Display2{get;set;}
    public Integer i = 0;
    Id id = ApexPages.currentPage().getParameters().get('id');
    public List<contact> lstCont = new List<Contact>();
    
    public CheckboxClass(Apexpages.Standardcontroller controller) {
        m_Display = false;
        m_Display2 = true;
    } 

    public PageReference delete1() {
        List<Contact> sEmails = new List<Contact>();
        for(WrapperClassEx cRef : getWrapperObj()) {
            if(cRef.cBox == true) {
                sEmails.add(cRef.cObj);
                System.debug('trueeeeeeeeeeeeeee');
            }
        }
        delete sEmails;
        PageReference p = new PageReference('/apex/ContactsInAccountExten'+'?'+'id'+'='+id);
        p.setRedirect(true);
        return p;
    }

   
    
    
        
    
    public List<WrapperClassEx> wrapList{get;set;}


    
    public List<WrapperClassEx> getWrapperObj() {
        if(wrapList == null) {
            wrapList = new List<WrapperClassEx>();
           // Account a = [SELECT id FROM Account WHERE id =: ApexPages.currentPage().getParameters().get('id')];
            for(Contact c : [SELECT id, lastname, AccountId FROM Contact WHERE AccountId =: ApexPages.currentPage().getParameters().get('id')]) {
                wrapList.add(new WrapperClassEx(c, false));
            }
        }
        return wrapList;
    }
    
    
    
    public class WrapperClassEx {
        public Contact cObj{get;set;}
        public Boolean cBox{get;set;}
        public Id id{get;set;}
        public Integer i;
             
        public WrapperClassEx(Integer i) {
            this.i = i;
            cObj = new Contact();
            //0memberAddList.add(new Contact());
        }
        public WrapperClassEx(Contact cObj, Boolean cBox) {
            this.cObj = cObj;
            this.cBox = cBox;
            
        }
    }
 
}