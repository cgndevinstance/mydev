public with sharing  class CustomerEntryPageController {
    public Account customerObj{get;set;}
    String customerId{get;set;}
    public String message{get;set;}
    public Boolean displayPopup {get;set;}
 
    
   public CustomerEntryPageController () { 
      message = '';
      
       
         customerObj = [SELECT Id,Name,TotalAmount__c FROM Account WHERE id=:Apexpages.currentpage().getParameters().get('id')];
      
   }
   public void showPopup()
    {
        
    displayPopup = true;

    
    }
    public void closePopup() {
        displayPopup = false;
        
    }
    
  
   public PageReference savingCustomerRecord(){
      try{
         upsert customerObj;
         PageReference nextpage= new PageReference('/'+customerObj.id);
           return nextpage;
      }
      catch(Exception e){
            message='Data Base error during saving...';
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, message));
            return null;
        }
   }
}