public class wrapperClassController
{
    private final Integer pageSize = 10;    

    
    public List<cContact> contactList
    {
        get
        {
            if ( contactList == null )
            {
                contactList = new List<cContact>();
                for ( Contact c : [SELECT Id, Name, Email, Phone FROM Contact] )
                {   
                    contactList.add( new cContact( c ) );
                }
            }
            return contactList;
        }
        private set;
    }

    private Integer pageNumber;

    private List<List<cContact>> list_Pages
    {
        get
        {
            if ( list_Pages == null )
            {
                list_Pages = new List<List<cContact>>();
                Integer numInPage = pageSize;
                List<cContact> thePage;
                for ( cContact cCon : contactList )
                {
                    if ( numInPage >= pageSize )
                    {
                        thePage = new List<cContact>();
                        list_Pages.add( thePage );
                        numInPage = 0;
                    }
                    thePage.add( cCon );
                    numInPage++;
                }
            }
            return list_Pages;
        }
        private set;
    }

    public  List<cContact> currentPage  { get { return list_Pages[ pageNumber ]; } }

    public  Boolean hasPrevious         { get { return pageNumber > 0; } }
    public  Boolean hasNext             { get { return pageNumber < list_Pages.size() - 1; } }

    public  void previousPage()         { if ( hasPrevious  ) pageNumber--; }
    public  void nextPage()             { if ( hasNext      ) pageNumber++; }

    public  wrapperClassController()
    {
        pageNumber = 0;
    }

   public PageReference processSelected()
    {
        List<Contact> selectedContacts = new List<Contact>();
        for ( cContact cCon : contactlist )
        {
            if ( cCon.selected ) selectedContacts.add( cCon.con );
        }
        for ( Contact con : selectedContacts )
        {
            system.debug('1111111111'+ con );
        }
        
        contactList = null;
        list_Pages  = null;
        return null;
    }

    public class cContact
    {
        public Contact con      { get; set; }
        public Boolean selected { get; set; }
        public cContact( Contact c )
        {
            con         = c;
            selected    = false;
        }
    }
}