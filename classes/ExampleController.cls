public class ExampleController {

    public DateTime t1{get;set;}
    public static DateTime t3{get;set;}
    public transient DateTime t2{get;set;}

    public void getTime() {
        if (t1 == null) t1 = System.now();
        
        if (t2 == null) t2 = System.now();
        
        if (t3 == null) t3 = System.now();
        
    }

}