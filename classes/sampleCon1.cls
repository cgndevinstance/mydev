public class sampleCon1 {
    
    public string selectedValue {get;set;} 
    public samplecon1() {
        selectedValue = 'False';
    }  
    public PageReference test() {
        selectedValue = 'True';
       
        return null;
    }
    public void  check() {
        selectedValue = 'True';
    }          
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('True','True')); 
        options.add(new SelectOption('False','False')); 
        return options; 
    }   
}