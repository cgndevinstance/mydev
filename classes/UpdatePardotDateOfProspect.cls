global class UpdatePardotDateOfProspect implements Database.Batchable<sObject> ,Database.AllowsCallouts 
{
    Set<ID> prospectIDsToUpdate = new Set<ID>();
    
    global UpdatePardotDateOfProspect(Set<ID> recentlyInsertedLeadIDS)
    {
        prospectIDsToUpdate = recentlyInsertedLeadIDS;
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT ID,Email from Lead WHERE ID in:prospectIDsToUpdate';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Lead> scope)
    {
         Pardot_Credentials__c credentials = [select Email__c,password__c,user_id__c from Pardot_Credentials__c where Email__c!=NULL];
         String email = credentials.Email__c;
         String password = credentials.password__c;
         String user_id = credentials.user_id__c;
        
         string endpoint = 'https://pi.pardot.com/api/login/version/3?email='+email+'&password='+password+'&user_key='+user_id;
         HttpRequest req = new HttpRequest();
         req.setEndpoint(endpoint);
         req.setMethod('POST');
         Http h = new Http();
         HttpResponse res = h.send(req);
         system.debug('responseBody'+res.getBody());
        
         Dom.Document doc = res.getBodyDocument();
         Dom.XMLNode address = doc.getRootElement();
         String api_key = address.getChildElement('api_key', null).getText();
        
     
         for(Lead leadRecord : scope)
         {  
             String endpointToUpdate = 'https://pi.pardot.com/api/prospect/version/3/do/update/email/'+leadRecord.Email+'?user_key='+user_id+'&api_key='+api_key+'&Salesforce_Added_Date='+system.now();
             
             HttpRequest reqToUpdate = new HttpRequest();
             reqToUpdate.setEndpoint(endpointToUpdate);
             reqToUpdate.setMethod('POST');
             Http httpToUpdate = new Http();
             HttpResponse restoUpdate = httpToUpdate.send(reqToUpdate);
             system.debug('UpdatedEmailOfPardot'+leadRecord.email);
         }
    } 
    
    global void finish(Database.BatchableContext BC)
    {
    }
}