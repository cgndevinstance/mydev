public class UpdateEligibleContactsController {
    public void checkContactsEligible(Set<Id> contactIds) {
        Set<Id> accountIds = new Set<Id>();
        Map<Id,List<Opportunity>> accOpportunityMap = new Map<Id,List<Opportunity>>();
        //get Accoutnts based on ContactId
        List<Contact> contactList = [Select AccountId,Eligible__c from Contact WHERE id in:contactIds];
        for(Contact con : contactList) {
            accountIds.add(con.AccountId);
        }
        system.debug('accountIdsaccountIds'+accountIds);
        //getting opportunites from Account
        List<Opportunity> oppList = [Select AccountId,Amount from Opportunity WHERE AccountId in:accountIds];
        //Constructing map to avoid query in loop
        for(Opportunity opp : oppList) {
            Id accountId = opp.AccountId;
            List<Opportunity> accOppList = accOpportunityMap.get(accountId);
            if(accOppList == null) {
                accOpportunityMap.put(accountId,new List<Opportunity>{opp});
            } else {
                accOpportunityMap.get(accountId).add(opp);
            }
        }
        List<Contact> updateContactList = new List<Contact>();
        for(Contact con : contactList) {
            Id accountId = con.AccountId;
            List<Opportunity> accountoppList = accOpportunityMap.get(accountId);
            Decimal totalSum = 0;
            for(Opportunity opp:accountoppList){
                Decimal amount = opp.Amount;
                totalSum += amount;
            }
            if(totalSum > 5000) {
                con.Eligible__c = true;
            } else {
                con.Eligible__c = false;
            }
            updateContactList.add(con);
        }
        if(updateContactList.size() > 0) {
            update updateContactList;
        }
        
        
        
    }
}