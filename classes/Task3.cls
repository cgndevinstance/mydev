/*    CreateBy        :   Ch.V.GopiNadh
        Functionality   :   Account
        Date            :   28/12/2013  
        Related class   :   Task2
        Description     :   displaying list of accounts group by picklist value in visualforce page */


public with sharing class Task3 {
	public List<Account> accounts{get;set;}
	public String[] types{get;set;}
	public Task3(){
		accounts=[select name,type from Account];
		set<String> t = new Set<String>();
		for(Account a : accounts){
			t.add(a.type);
		}
		types =new String[t.size()];
		Integer i=0;
		 for(String ty : t){
			types[i]=ty;
			i++;
		}
		
	} 
}