public class parseCSV
{   
    public static List<List<String>> parseCSV(String contents, Boolean skipHeaders)
    {
        List<List<String>> allFields = NEW List<List<String>>();
    
        List<String> lines = NEW List<String>();
        
        try {
        
            lines = contents.split('\n');
        
        } catch (System.ListException e) {
        
            System.debug('Limits exceeded?' + e.getMessage());
        }
        
        Integer num = 0;
        for(String line : lines) {
            if (line.replaceAll(',','').trim().length() == 0) break;
            
            List<String> fields = line.split('","');  
            
            List<String> cleanFields = new List<String>();
            
            for(String field : fields) {
                if (field.endsWith('"'))
                    field = field.removeEnd('"');
                
                if (field.startsWith('"'))
                    field = field.removeStart('"');
                
                field = field.replace(',', 'COMMA');
                
                cleanFields.add(field );
            }
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }
 
    public static list<sObject> csvTosObject(List<List<String>> parsedCSV, string objectType)
    {
        Schema.sObjectType objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType();
        
        list<sObject> objects = new list<sObject>();
        list<string> headers = new list<string>();
        
        for(list<string> row : parsedCSV)
        {
            
            for(string col : row)
            {
                headers = col.split('COMMA');
            }
            break;
        }
        system.debug(headers);
            
        integer rowNumber = 0;
        for(list<string> row : parsedCSV)
        {
            if(rowNumber == 0)
            {
                rowNumber++;
                continue;
            }
            else
            {
                sObject thisObj = objectDef.newSobject();
                integer colIndex = 0;
                for(string col : row)
                {                   
                    string headerName = headers[colIndex].trim();
                    if(headerName.length() > 0)
                    {                  
                        try
                        {                       
                            thisObj.put(headerName, col.trim().replace('COMMA', ',').removeEnd('"'));
                        }
                        catch(exception e)
                        {
                        }
                        colIndex++;
                    }
                } 
                objects.add(thisObj);
                rowNumber++;
            }       
        }
        return objects;
    }
    
}