public class PositionTriggerHandler{
    private static PositionTriggerHandler instance; 
    public static Boolean isExecute = false;
    public static PositionTriggerHandler getInstance() {
        if(instance == null){
            instance = new PositionTriggerHandler();
        }
        return instance;
    }
    public static void handleBeforeInsert(List<Position__c> newPositionList) {
        System.debug('handleBeforeInsert method');
    }
    public static void handleAfterInsert(List<Position__c> newPositionList) {
       if(!PositionTriggerHandler.isExecute) {
       PositionTriggerHandler.isExecute = true;
       Position__c pos = new Position__c();
       pos.name  = 'Test Position';
       insert pos;
       System.debug('handleAfterInsert after insert');
       }
       System.debug('handleAfterInsert after insert after if');
    
    }
    public static void handleBeforeUpdate(List<Position__c> newPositionList, Map<Id,Position__c> oldPositionMap) {
       System.debug('Before update method');
    }
    public static void handleAfterUpdate(List<Position__c> newPositionList, Map<Id,Position__c> oldPositionMap) {
        System.debug('After update method');
        if(!PositionTriggerHandler.isExecute) {
            PositionTriggerHandler.isExecute = true;
            System.debug('handleAfterInsert after update');
        }
        System.debug('handleAfterInsert after update after if');
    }
}