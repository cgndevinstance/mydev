global class BatchProcessing implements Database.Batchable<sObject> ,Database.AllowsCallouts,Database.Stateful
{
   Map<Id,String> Tempbodies = new Map<Id,String>();
   global Database.QueryLocator start(Database.BatchableContext BC)
    {
       String query = 'SELECT Id,Name,body From EmailTemplate where IsActive=true limit 1';
        
        return Database.getQueryLocator(query); 
    }
   
    global void execute(Database.BatchableContext BC,List<EmailTemplate> scope)
    {   
          // Map<Id,String> Tempbodies = new Map<Id,String>();
          
           Map<Id,Messaging.SingleEmailMessage> MailMap = new Map<Id,Messaging.SingleEmailMessage>();
           List<Messaging.SingleEmailMessage> listSingleEmailMessages = new List<Messaging.SingleEmailMessage>();
           Opportunity opp = [select id,Contact_Name__c from Opportunity where id=:'0069000000OVXGT'];
           for(EmailTemplate e:scope){
               Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
               mail.setWhatId(opp.id);
               //System.debug('aaaaaaaaaaaa'+opp.Contact_Name__c);    
               mail.settargetObjectId(opp.Contact_Name__c);
               mail.setTemplateId(e.id); 
               String[] toAddresses = new String[] {'ravi@tvarana.com'};
               mail.setToAddresses(toAddresses);
               listSingleEmailMessages.add(mail);
               MailMap.put(e.id,mail);
           }
           //Savepoint sp = Database.setSavepoint();
           Messaging.sendEmail(listSingleEmailMessages); 
           //Database.rollback(sp);
           listSingleEmailMessages.clear();
           for(EmailTemplate et:scope){
               Messaging.SingleEmailMessage sem = MailMap.get(et.id);
               Tempbodies.put(et.id,sem.getPlainTextBody());
           }
              
    }   
    global void finish(Database.BatchableContext BC)
    {
       List<EmailTemplate> templist = [SELECT Id,Name,body From EmailTemplate where IsActive=true];
       system.debug('qqqqqqqqqqqq2'+ Tempbodies.get(templist[0].id));
       for(EmailTemplate e : templist){
           Store_Amazon_Data__c store = new Store_Amazon_Data__c();
           store.Template_Id__c = e.id;
           store.Email_Template_Body__c = Tempbodies.get(e.id);
           //insert store;
       }
       FromBatchProcessingClass batch = new FromBatchProcessingClass();
       //Database.executeBatch(batch);
       /*Amazon_SES_SendEmail sendemail = new Amazon_SES_SendEmail();
       sendemail.sendEmailFromAmazon('gopinath@tvarana.com','gopinath@tvarana.com',
                                        'Approval_process_based_on_Amount',Tempbodies.get('body'),'text');*/
    }
}