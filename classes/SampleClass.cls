public class SampleClass {
    
    public String m_LastName{get;set;}
    public String m_Email{get;set;}
    public String m_Phone{get;set;}
    public Contact objContact{get;set;}
    public Boolean m_Display{get;set;}
    public Boolean m_Display2{get;set;}
    public Integer i = 0;
    Id id = ApexPages.currentPage().getParameters().get('id');
    public List<contact> lstCont = new List<Contact>();
    
    public SampleClass(Apexpages.Standardcontroller controller) {
        m_Display = false;
        m_Display2 = true;
    }

    public void add() {
        m_Display = true;
        m_Display2 = false;
    }
    
    public PageReference back() {
        PageReference p = new PageReference('/apex/ContactsInAccountExten'+'?'+'id'+'='+id);
        p.setRedirect(true);
        return p;
    }

    public PageReference insert1() {
        if(objContact == null) {
            objContact = new Contact();
            objContact.AccountId = id;
            objContact.lastName = m_LastName;
            objContact.Email = m_Email;
            objContact.Phone = m_Phone;
            insert objContact;
        }
        PageReference p = new PageReference('/apex/ContactsInAccountExten'+'?'+'id'+'='+id);
        p.setRedirect(true);
        return p;
    }

    public PageReference update1() {
        List<Contact> sEmails = new List<Contact>();
        for(WrapperClassEx cRef : getWrapperObj()) {
            if(cRef.cBox == true) {
                sEmails.add(cRef.cObj);
            }
        }
        update sEmails;
        PageReference p = new PageReference('/apex/ContactsInAccountExten'+'?'+'id'+'='+id);
        p.setRedirect(true);
        return p;
    }

    public PageReference delete1() {
        List<Contact> sEmails = new List<Contact>();
        for(WrapperClassEx cRef : getWrapperObj()) {
            if(cRef.cBox == true) {
                sEmails.add(cRef.cObj);
            }
        }
        delete sEmails;
        PageReference p = new PageReference('/apex/ContactsInAccountExten'+'?'+'id'+'='+id);
        p.setRedirect(true);
        return p;
    }

    public PageReference delete2() {
        Id sid = ApexPages.currentPage().getParameters().get('uid');
        System.debug('11111111111111111111111111'+sid);
        Contact c = [SELECT Id FROM Contact WHERE Id =: sid ];
        delete c;
        PageReference p = new PageReference('/apex/ContactsInAccountExten'+'?'+'id'+'='+id);
        p.setRedirect(true);
        return p;
    }
    
    
    public PageReference insert2() {
        for(Integer j=0; j<wrapList.size(); j++) {
        System.debug('++++++++++++++++++++++WRAPLIST INSERT2'+wrapList[j].cObj);
            wrapList[j].cObj.accountId = ApexPages.currentPage().getParameters().get('id');
            lstCont.add(wrapList[j].cObj);
        }
        System.debug('*********************LISTCONTACT INSERT2'+lstCont);    
        upsert lstCont;
        PageReference p = new PageReference('/apex/ContactsInAccountExten'+'?'+'id'+'='+id);
        p.setRedirect(true);
        return p;
    }
    
    
    public List<WrapperClassEx> wrapList{get;set;}


    
    public List<WrapperClassEx> getWrapperObj() {
        if(wrapList == null) {
            wrapList = new List<WrapperClassEx>();
           // Account a = [SELECT id FROM Account WHERE id =: ApexPages.currentPage().getParameters().get('id')];
            for(Contact c : [SELECT id, lastname, email, phone, AccountId FROM Contact WHERE AccountId =: ApexPages.currentPage().getParameters().get('id')]) {
                wrapList.add(new WrapperClassEx(c, false));
            }
        }
        return wrapList;
    }
    
    public void add1() {
        i = i++;
        add2();
    }
    public void add2() {
        WrapperClassEx w = new WrapperClassEx(i);
        wrapList.add(w);
        System.debug('************WRAPLIST ADD2'+wrapList);
    }
    
    public class WrapperClassEx {
        public Contact cObj{get;set;}
        public Boolean cBox{get;set;}
        public Id id{get;set;}
        public Integer i;
             
        public WrapperClassEx(Integer i) {
            this.i = i;
            cObj = new Contact();
            //0memberAddList.add(new Contact());
        }
        public WrapperClassEx(Contact cObj, Boolean cBox) {
            this.cObj = cObj;
            this.cBox = cBox;
            
        }
    }
}