public with sharing class RenewalOpportunityController {
    public Captain__c SalesorderItem{get;set;}
    public Decimal UpliftPercentage{get;set;}
    public Id JobId{get;set;}
    public Integer JobItemsProcessed{get;set;}
    public Integer TotalJobItems{get;set;}
    public Integer NumberOfErrors{get;set;}
    public string status{get;set;}
    public RenewalOpportunityController() {
        SalesorderItem = new Captain__c();
        
    }
    public void GenerateOpps() {
        if ( SalesorderItem.Subscription_Start_Date__c != null && SalesorderItem.Subscription_End_Date__c != null) {
            RenewalEngineBatchController RenewalBatch = new RenewalEngineBatchController(SalesorderItem.Subscription_Start_Date__c,SalesorderItem.Subscription_End_Date__c,UpliftPercentage);
            JobId = Database.executeBatch(RenewalBatch,1);
           BatchStatus();
        }
    }
    public void BatchStatus() {
        List<AsyncApexJob> job = [SELECT Id,status,JobItemsProcessed,NumberOfErrors,TotalJobItems
                                FROM AsyncApexJob
                                WHERE id =: JobId];
        System.debug('ssssssssssss'+jobid);
        
        if (job.size() > 0) { 
           status = job[0].status;
           TotalJobItems = job[0].TotalJobItems;
           JobItemsProcessed = job[0].JobItemsProcessed;
           NumberOfErrors = job[0].NumberOfErrors;
        }
    }
}