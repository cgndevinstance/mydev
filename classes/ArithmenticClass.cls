public with sharing class ArithmenticClass {
    
    public Double firstvalue{get;set;}
    public Double secondvalue{get;set;}
    public Double result{get;set;}
    
    
    public void add(){
        
        result=firstvalue + secondvalue;
        
    }
    public void sub(){
        
        result=firstvalue - secondvalue;
        
    }
    public void mul(){
        
        result=firstvalue * secondvalue;
        
    }
    public void div(){
        
        result=firstvalue / secondvalue;
        
    }
}