public class GeneareTemlateStructure {
    public void generatingTemplate() {
        List<Contact> contactList = [select lastname,Email from contact limit 2];
        EmailTemplate temp = [select htmlvalue,subject from EmailTemplate where id='00X90000001rKVd'];
        string htmlBody = temp.htmlValue;
        string prevBody = htmlBody.substringBefore('</table>'); 
        for ( contact con : contactList ) {
            string body = '<tr><td>'+con.LastName+'</td><td>'+con.Email+'</td></tr>';
            prevBody = prevBody + body;
        }
        prevBody = prevBody + '</table>';
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        // Set recipients to two contact IDs.
        // Replace IDs with valid record IDs in your org.
        message.toAddresses = new String[] { 'gopinath@tvarana.com' };
        message.optOutPolicy = 'FILTER';
        message.subject = temp.subject;
        message.setHtmlBody(prevBody);
        Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                 Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                  + results[0].errors[0].message);
        }
                
    }
}