public with sharing class FirstController {
	public Account a{get;set;}
	public FirstController(){
		
			Id id=Apexpages.currentpage().getParameters().get('id');
		
			if(id==null){
				a=new Account();
			}	
	     	else{
	     		a=[select name from account where id=:id];
	     	}
		
		
	}
	public pagereference save(){
		
		upsert a;
		pagereference p=new pagereference('/'+a.id);
		return p;
	}
	
}