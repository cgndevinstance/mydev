public with sharing class updateContactNamesHelper {

    public static void updateContactName ( Map <ID, Account> oldAccountsMap,
                                        Map <ID, Account> newAccountsMap ) {
    
        Set <ID> updatedAccountIdsSet = NEW Set <ID> ();
        List <Contact> contactsRelatedToAccounts = NEW List <Contact> ();
        List <Contact> contactsToUpdate = NEW List <Contact> ();
        
        for (ID accountID: newAccountsMap.keySet ()) {
            if (oldAccountsMap.get (accountID).Name != newAccountsMap.get (accountID).Name) {
                updatedAccountIdsSet.add (accountID);
            }
        }                        
        
        
        contactsRelatedToAccounts = [ SELECT 
                                            Name, AccountID
                                        FROM
                                            Contact
                                        WHERE
                                            AccountID IN: updatedAccountIdsSet ];
                                            
        for (Contact con: contactsRelatedToAccounts) {
            con.firstName = NULL;
            con.LastName = newAccountsMap.get (con.AccountID).Name;
            contactsToUpdate.add (con);
        }
        
        if (!contactsToUpdate.isEmpty())
            DataBase.Update (contactsToUpdate, false);
    }
                                        
}