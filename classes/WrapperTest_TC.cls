@istest(SeeAllData = True)
private class WrapperTest_TC{
    static testmethod void method1(){
        WrapperTest wt = new WrapperTest();
        wt.login();
        wt.next();
        wt.skip();
        wt.reset();
        wt.prev();
        wt.start();
        wt.starttechncial();
        wt.getAnswer();
        wt.setAnswer('A');
        wt.uname = 'TVA1182014005';
        wt.pwd = 'cgn@gmail.com';
        Question__c q= new Question__c();
        q.Option1__c = 'A';
        q.Option2__c = 'B';
        q.Option3__c = 'A';
        q.Option4__c = 'A';
        q.Option5__c = 'A';
        WrapperTest.WrapperClassEx we = new WrapperTest.WrapperClassEx(q,'A');
        we.getAnswers();
        wt.submit();
    }
    static testmethod void method2(){
        WrapperTest wt = new WrapperTest();
        wt.login();
        Question__c q= new Question__c();
        q.Option1__c = 'A';
        q.Option2__c = 'B';
        q.Option3__c = 'A';
        q.Option4__c = 'A';
        q.Option5__c = 'A';
        for(Integer i=0;i<=9;i++){
            wt.counterindex = i;
            wt.next();
            wt.skip();
        }
    }
}