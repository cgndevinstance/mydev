public with sharing class SiteLeadCreateController {
    
    public String msg { set; get; }
    public String key{ set; get; }
    public Map<string,set<string>> selectedTweetsMap;
    public Map<string,List<myLeadTweets>> allUserTweets{get;set;}
    public Map<string,string> userNamesMap{get;set;}
    public Integer recCount{get;set;}
    public Boolean showLink{get;set;}
    public Integer diff{get;set;}
    public string customMessage{get;set;}
    public Integer counter{get;set;}
    public Integer selectedTweetsCount{get;set;}
    public void init() {
        userNamesMap = new Map<string,string>();
        allUserTweets = new Map<string,List<myLeadTweets>>();
        selectedTweetsMap = new Map<string,set<string>>();
        recCount = 0;
        showLink = False;
        diff = 0;
        counter = 7;
        customMessage = 'view 1 new tweet';
        selectedTweetsCount = 0;
        getLeadTweets();
    }
    
    public void getLeadTweets() {
        String ownerId = Userinfo.getUserId();
        
        String keyencoded = EncodingUtil.urlEncode('Qm7DikKf4vZm7jscT0MezVMT1','UTF-8');
        String secretkeyencoded = EncodingUtil.urlEncode('YXb6YUWPCRBpZnqqV01COJtY2yoi6MlIgHA70Oo1wrE3DSUfxX', 'UTF-8');
        String sFinal = keyencoded + ':' + secretkeyencoded;
        Blob headerValue = Blob.valueOf(sFinal);
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.twitter.com/oauth2/token');
        req.setMethod('POST');
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setBody('grant_type=client_credentials');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        string access_token = '';
        string tweets = '';
        JSONParser parser = JSON.createParser(res.getBody());
        while(parser.nextToken() != null)
        {
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME)
            {
                String fieldName = parser.getText();
                parser.nextToken();
                if(fieldName == 'access_token')
                {
                    access_token = parser.getText();
                }
    
            }
        }
        List<Twitter_logins__c> twitterLogins = Twitter_logins__c.getAll().values();
        for ( Twitter_logins__c userName : twitterLogins ) {
            List<myLeadTweets> myTweets = userTweets(userName.Name,access_token);
            userNamesMap.put(userName.Name,userName.Login_Name__c);
            allUserTweets.put(userName.Name,myTweets);
        }
        
        Integer totalCount = 0;
        for ( List<myLeadTweets> values : allUserTweets.values() ) {
            totalCount += values.size();
        }
        system.debug('totalCounttotalCount'+totalCount);
        system.debug('recCountrecCount'+recCount);
        if ( recCount == 0 ) {
            recCount = totalCount;
        } else {
            if ( recCount != totalCount ) {
                showLink = True;
                if ( totalCount > recCount ) {
                    diff = totalCount - recCount;
                    if ( diff == 1 ) 
                        customMessage = 'View '+diff+' new Tweet';
                    else 
                        customMessage = 'View '+diff+' new Tweets';
                } else {
                    diff = recCount - totalCount;
                    if ( diff == 1 ) 
                        customMessage = diff+' Tweet Deleted';
                    else 
                        customMessage = diff+' Tweets Deleted';
                }
                
                recCount = totalCount;
                counter = counter + 20;
            } 
        }
        system.debug('showLinkshowLink'+showLink);
        
    }
    public void hideLink() {
        customMessage = '';
        showLink = False;   
        getLeadTweets();
    }
    public List<myLeadTweets> userTweets(string userid,string access_token) {
        List<myLeadTweets> tweetedLeads = NEW List<myLeadTweets>();
        HttpRequest req2 = new HttpRequest();
        req2.setEndpoint('https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name='+userid);
        req2.setMethod('GET');
        String authorizationHeader2 = 'Bearer ' + access_token;
        req2.setHeader('Authorization', authorizationHeader2);
        Http http1 = new Http();
        HTTPResponse res1 = http1.send(req2);
        string tweets = res1.getBody();
        System.debug('tweetstweets'+tweets);
        
        if ( tweets != '' ) {
            List<Object> root = (List<Object>)JSON.deserializeUntyped(tweets);
            for (Object indivObject: root) {
                Map<String, Object> obj = (Map<String, Object>)indivObject;
                tweetedLeads.add(new myLeadTweets(string.valueOf(obj.get('text'))));
            }
            
        }
        System.debug('userNamesMapuserNamesMap'+userNamesMap);
        return tweetedLeads;
    }
    
    public PageReference updateMapIds() {
        //count++;
        Boolean isAdd = Boolean.ValueOf(Apexpages.currentPage().getParameters().get('event'));
        System.debug('isAddisAdd'+isAdd);
        if(isAdd){
            if ( selectedTweetsMap.containskey(key) ) {
                selectedTweetsMap.get(key).add(msg);
            } else {
                selectedTweetsMap.put(key,new set<string>{msg});
            }
            selectedTweetsCount++;
           
        } else {
            if ( selectedTweetsMap != null ) {
                if ( selectedTweetsMap.get(key) != null ) {
                    selectedTweetsMap.get(key).remove(msg);   
                }
            }
                
            selectedTweetsCount--;
        }
        
        System.debug('selectedTweetsMapselectedTweetsMap2222'+selectedTweetsMap);
        return null;
    } 
    public void SendMails() {
        campaign cam = [select id from campaign where name = 'Twitter Mailing List'];
        List<CampaignMember> campaignMembers = [Select contactid From CampaignMember where campaignid=:cam.id];
        set<Id> conIds = new set<id>();
        List<string> toAddresses = new List<string>();
        for (CampaignMember member : campaignMembers ) {
            conIds.add(member.contactid);
        }
        System.debug('selectedTweetsMapselectedTweetsMap'+selectedTweetsMap);
        List<contact> contactList = [select Email from contact where id in:conIds];
        
        if ( contactList.size() > 0 ) {
            for ( contact con : contactList) {
                if ( con.Email != null ) {
                    toAddresses.add(con.Email);
                }
            }
        } else {  
            toAddresses.add('gopinadh.chinta@gmail.com');
            toAddresses.add('chvgopinadh@gmail.com');
            toAddresses.add('gopinath@tvarana.com');
            toAddresses.add('susundar@athenahealth.com');
            toAddresses.add('‪kudaykumar@athenahealth.com');
            
        } 
        
        if ( selectedTweetsMap != null ) {
            List<Lead> leadList = new List<Lead>();
            string tweetBody = '';
            for ( string key: selectedTweetsMap.keyset() ) {
                for ( string msg : selectedTweetsMap.get(key) ) {
                    Lead lead = new Lead();
                    lead.LastName = key;
                    lead.company = key;
                    lead.description = msg;
                    tweetBody += key +' ---- '+ msg;
                    tweetBody += '\n';
                    leadList.add(lead);
                }
            }
            if ( leadList.size() > 0 ) {
                //insert leadList;
                database.insert(leadList,false);
            }
            system.debug('toAddressestoAddresses'+toAddresses);
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = toAddresses;
            message.subject = 'Tweeted Texts';
            message.plainTextBody = tweetBody;
            try {
                Messaging.SingleEmailMessage[] messages = 
                    new List<Messaging.SingleEmailMessage> {message};
                         Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                if (results[0].success) {
                    System.debug('The email was sent successfully.');
                } else {
                    System.debug('The email failed to send: '
                          + results[0].errors[0].message);
                }
            } catch ( EmailException ex ) {
            
            }
        }
        
    }
    public class myLeadTweets {
        public Boolean isSelected { set; get; }
        public string tweet{get;set;}
        public myLeadTweets(string tweet) {
            isSelected = false;
            this.tweet = tweet;
            
        }
    }
}