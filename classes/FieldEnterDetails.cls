public class FieldEnterDetails {
        public Account acc;
        private Apexpages.StandardController stdcontroller;
        public FieldEnterDetails(ApexPages.StandardController controller){
                this.stdcontroller = controller;
        }
        public Account getData(){
                if(acc == null ){
                    acc= new Account();
                }
                return acc;
        }
         public  PageReference secondpage(){
                 return page.DisplayGenderAndMaritalStatus;
         }
         public  PageReference thirdpage(){
                 return Page.ConfirmationDetails;
         }
         public PageReference submit(){
                 stdcontroller.save();
                 return page.Thankyoupage;
         }
        public PageReference modify(){
                return page.DisplayNameandsalaryPage;
        }
        public PageReference back(){
                return page.DisplayNameandsalaryPage;
        }
}