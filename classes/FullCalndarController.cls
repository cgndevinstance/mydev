public class FullCalndarController{
    public string EventBody{get;set;}
    public string ResourceBody{get;set;}
    public string EventId{get;set;}
    public string day{get;set;}
    public string month{get;set;}
    public string year{get;set;}
    public string ResId{get;set;}
    
    public Map<string,Task> EventMap = new Map<string,Task>();
   public FullCalndarController() {
        FetchTaskEvents();
    }
    public void FetchTaskEvents () {
        EventBody = ' ';
        ResourceBody = '';
        Map<String,string> EventUsersMap = new Map<String,string>();
        List<Task> EventList = [SELECT Id,OwnerId,Subject,Status,Owner.Name, ActivityDate from Task where owner.isActive = true ];
        System.debug('EventListEventList'+EventList);
        for ( Task evnt : EventList ) {
             if ( evnt.ActivityDate != null ) {
                 EventMap.put(evnt.id,evnt);
                 EventBody += '{';
                 EventBody += 'title: \''+evnt.Subject+'\'  , ';
                 string s = string.valueOf(evnt.ActivityDate);
                 if ( s.contains(' ') ) {
                     s = s.substringBefore(' ');
                 }
                 system.debug('eeeeeeeeee'+s);
                 Date Actdate = Date.valueOf(s);
                 EventBody += 'start: \''+s+'\'  , ';
                 EventBody += 'resourceId: \''+evnt.OwnerId+'\'  , ';
                 EventBody += 'allDay: true  , ';
                 EventBody += 'id: \''+evnt.id+'\'';
                 EventBody += '},';
                 
                 ResourceBody += '{';
                 ResourceBody += 'id: \''+evnt.OwnerId+'\'  , ';
                 ResourceBody += 'eventTextColor:\''+'red'+'\'  , ';
                 ResourceBody += 'eventColor:\''+'white'+'\'  , ';
                 ResourceBody += 'title: \''+evnt.Owner.Name+'\'';
                 ResourceBody += '},';
             }
         }
         EventBody = EventBody.substringBeforeLast(',');
         ResourceBody = ResourceBody.substringBeforeLast(',');
         system.debug('EventBodyEventBody'+EventBody);
          system.debug('ResourceBody'+ResourceBody);
         
    }
    public void updateEvent(){
        System.debug('EventIdEventId'+EventId+'---'+day+'---'+month+'---'+year);
        Task event = EventMap.get(EventId);
        event.ActivityDate = Date.valueOf(year+'-'+month+'-'+day);
        System.debug('rrrrrrrrrrr'+Resid);
        event.OwnerId = Resid; 
        update event;
    }
}