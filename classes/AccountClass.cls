public with sharing class AccountClass {

    public AccountClass(ApexPages.StandardController controller) {

    }

    public PageReference AccountButton() {
       Date d = Date.today();
       Integer year = d.Year();
       Integer month = d.Month();
       Integer day = d.Day();
       
       Account a = [select id,name,Gender__c,UpdateField__c,updateDate__c from Account where id =:ApexPages.currentPage().getParameters().get('id')];
       Integer updateyear = a.updateDate__c.year();
       Integer updatwmonth = a.updateDate__c.month();
       Integer updateday = a.updateDate__c.day();
       if(a.UpdateField__c != true){
           a.Gender__c = 'male';
           a.UpdateField__c = true;
           a.updateDate__c = Date.today();
           update a;
       }
       return null;
    }

}