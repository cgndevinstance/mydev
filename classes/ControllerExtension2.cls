public class ControllerExtension2 {
    public Account acc{get;set;}
    public string greeting{get;set;}
    public ControllerExtension2(ApexPages.StandardController controller) {
        acc = (Account)controller.getRecord();
        method1();
    }

    
    public void method1() {
       greeting = 'Hello ' + acc.name + ' (' + acc.id + ')';

    }
}