public with sharing class MassEmailController
{
    public string ObjectName{get;set;}
    public string FolderId{get;set;}
    public String TempId{get;set;}
    public string TemplateId{get;set;}
    public String Emailbody{get;set;}
    public List<SelectOption> TemplatesPickList{get;set;}
    public set<Id> EmailTemplateIds = new set<Id>();
    public MassEmailController(){
        
        //system.debug('Templates Templates '+Templates );
    }
    
    
    
    public List<SelectOption> PickListForObjects = new List<SelectOption>();
    
    public List<SelectOption> getPickListForObjects()
    {
        
        List<SelectOption> PickListForObjects1 = new List<SelectOption>();
        PickListForObjects1.add(new SelectOption('--None--', '--None--'));
        PickListForObjects1.add(new SelectOption('Contact', 'Contact'));
        PickListForObjects1.add(new SelectOption('Lead', 'Lead'));
        return PickListForObjects1;
    }
  /*  public List<SelectOption> setPickListForObjects()
    {
        
        List<SelectOption> PickListForObjects1 = new List<SelectOption>();
        PickListForObjects1.add(new SelectOption('Contact', 'Contact'));
        PickListForObjects1.add(new SelectOption('Lead', 'Lead'));
        return PickListForObjects1;
    } */
    public List<SelectOption> getFolderPickList()
    {
        List<SelectOption> PickListForObjects1 = new List<SelectOption>();
        List<Folder> FolderList = [select id,Name from Folder where type = 'Email'];
        
        system.debug('AAAAAAA'+FolderList );
        PickListForObjects1.add(new SelectOption('--None--', '--None--'));
        PickListForObjects1.add(new SelectOption('00D90000000q8RJ', 'Unfiled Public Email Templates'));
        PickListForObjects1.add(new SelectOption('00590000001h142', 'My Personal Email Templates'));
        
        for(Folder A:FolderList)
        {
            
            if(A.Name != null)
                PickListForObjects1.add(new SelectOption(A.id, A.Name));
        }
        return PickListForObjects1;
    }
    
    public void RetriveTemplates()
    {
       
       system.debug('FolderIdFolderId'+FolderId);
          TemplatesPickList = new List<SelectOption>();
          TemplatesPickList.add(new SelectOption('--None--', '--None--'));
        //Id FolderId = Id.valueOf(ApexPages.currentPage().getParameters().get('folderId'));
        List<EmailTemplate> AllTemplates = [select id,name from EmailTemplate where folderid =:FolderId];
        
        system.debug('AllTemplates AllTemplates '+AllTemplates );
        for(EmailTemplate Doc:AllTemplates)
        {
            TemplatesPickList.add(new SelectOption(Doc.id, Doc.Name));
        }
          
        system.debug('TemplatesPickListTemplatesPickList'+TemplatesPickList);
    }
    public void generateBody(){
        EmailTemplate d = [select id, body from EmailTemplate where id=:TempId];
        //String strBody = EncodingUtil.base64Encode( d.body );
        //Blob b = d.body;
        //Emailbody = b.toString();
        Emailbody = d.body;
        System.debug('bbbbbbbbbb'+Emailbody);
    }
    public void ObjectViewList()
    {
        
    }
}