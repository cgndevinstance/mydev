public class NetworkProductSearch1 
{
    public Boolean pb1 {get;set;}
    public Boolean pb2 {get;set;}

    public List<OpportunityLineItem> oliList {get; set;}
    public List<olWraps> olws {get;set;}
    public List<Network_Product_Search__c> nps {get;set;}
    public List<ProductsWithSelection> npsSelected {get;set;}
    public Boolean searchingPB{get;set;}
    public List<ProductsWithSelection> allProducts {get;set;}
    
    public List<SelectOption> cat1 {get;set;}
    public Set<String> cat1Selected {get;set;}
    public List<SelectOption> cat2 {get;set;}
    public List<String> cat2Selected {get;set;}
    public List<SelectOption> cat3 {get;set;}
    public List<String> cat3Selected {get;set;}
    public List<SelectOption> cat4 {get;set;}
    public List<String> cat4Selected {get;set;}
    public String selected1 {get; set;}
    public String selected2 {get; set;}
    public String selected3 {get; set;}
    public String selected4 {get; set;}
  
    public NetworkProductSearch1(ApexPages.StandardController controller) 
    {
        pb1 = true;
        pb2 = false;
        searchingPB = false;
        oliList = new List<OpportunityLineItem>();
        olws = new List<olWraps>();
        allProducts = new List<ProductsWithSelection>();
        //nps = Network_Product_Search__c.getAll().values();
        nps = [SELECT Name, Product_Name__c, Select_Category_1__c, Select_Category_2__c, Select_Category_3__c, Select_Category_4__c FROM Network_Product_Search__c ORDER BY Product_Name__c];
        for (Network_Product_Search__c n: nps)
        {   
            ProductsWithSelection objPWS = new ProductsWithSelection();
            objPWS.selected = false;
            objPWS.product = n;
            allProducts.add(objPWS);
        }
        
        npsSelected = allProducts;
        
        Set<String> cat1set = new  Set<String>();
                
        for(ProductsWithSelection n : allProducts)
        {
            cat1set.add(n.product.Select_Category_1__c);
        }
                
        cat1 = new List<selectOption>();
        cat1.add(new Selectoption('', '-None-'));
        for(String n : cat1set)
        {
            cat1.add(new Selectoption(n, n));
        }
       cat1.sort();
        cat2 = new List<selectOption>();
        cat2.add(new Selectoption('', '-None-'));
        cat3 = new List<selectOption>();
        cat3.add(new Selectoption('', '-None-'));
        cat4 = new List<selectOption>();
        cat4.add(new Selectoption('', '-None-'));
    }


    public PageReference doSelectAll() {
    
        for(ProductsWithSelection n: npsSelected )
        {
            n.selected = true;        
        }
    
        return null;
    }
    public PageReference doDeSelectAll() {
    
        for(ProductsWithSelection n: npsSelected )
        {
            n.selected = false;        
        }
    
        return null;
    }

    public PageReference addToOpportunity() {
        pb1 = false;
        pb2 = true;
        
        searchingPB = false;
        Id oppId = apexPages.currentPage().getParameters().get('id');
        Set<String> selectedProductNames = new Set<String> ();
       // List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        oliList = new List<OpportunityLineItem>();
        olws = new List<olWraps>();
        for(ProductsWithSelection n: npsSelected)
        {
            if(n.selected == true)
            selectedProductNames.add(n.product.product_name__c);
            
            }
        System.Debug('Selected Products --------- : ' + selectedProductNames);
        
        List<Product2> relatedProducts = [SELECT Id, name FROM Product2 WHERE name IN: selectedProductNames];
        
        System.Debug('Selected Products from Product --------- : ' + relatedProducts);
        
        Set<Id> relatedProductIds = new Set<Id>();
        for(Product2 p:relatedProducts)
         relatedProductIds.add(p.Id);
        System.Debug('Set of related Product Ids --------- : ' + relatedProductIds); 
         
         List<PriceBookEntry> priceBookList = [SELECT Id, Name, unitprice, Product2.Id, Product2Id, Product2.Name FROM PriceBookEntry WHERE Product2.Id IN: relatedProductIds AND PRICEBOOK2ID = '01s90000001c3y9'];
        
        for(PriceBookEntry record: priceBookList )
        {
        opportunitylineitem olObj = new opportunitylineitem(
                                                opportunityid = oppId,
                                                unitprice = record.unitprice,  
                                                quantity = 1,
                                                pricebookentryid = record.id,
                                                Discount = 5
                                            );
        olWraps olwObj = new olWraps();
        olwObj.productName = record.Name;
        olwObj.oli = olObj;                      
        oliList.add(olObj);
        olws.add(olwObj);
        System.Debug('PriceBookEntry Id ---------- : ' + record.id);
         }
        
        System.debug('Opp line items -------- : ' + oliList);                                      
     //   insert oliList;
        
      // return (new pagereference('/apex/quantityPage?oliList='+oliList).setredirect(false));
             
            // return (new pagereference('/oppitm').setredirect(false));
         //   return (new pagereference('/'+oppId).setredirect(false));
       return null;
    }
    
    public PageReference doSaveOLI() {
    insert oliList;
        Id oppId = apexPages.currentPage().getParameters().get('id');    
    return (new pagereference('/'+oppId).setredirect(false));
    
    
    }
    public PageReference doCancel() {
    Id oppId = apexPages.currentPage().getParameters().get('id');    
    return (new pagereference('/'+oppId).setredirect(false));
    
    
    }
 

    public PageReference doGetCat2() {  
        npsSelected = new List<ProductsWithSelection>();
        Set<String> opSet = new Set<String>();
        cat2 = new List<selectOption>();
        cat2.add(new Selectoption('', '-None-'));
        cat3 = new List<selectOption>();
        cat3.add(new Selectoption('', '-None-'));
        cat4 = new List<selectOption>();
        cat4.add(new Selectoption('', '-None-'));
        system.debug('selected1 ************'+selected1);
        for(ProductsWithSelection n : allProducts)
            {
             if(selected1 == n.product.Select_Category_1__c)
             npsSelected.add(n);
             if(selected1 == n.product.Select_Category_1__c && n.product.Select_Category_2__c != null && !opSet.contains(n.product.Select_Category_2__c))
                {
                 cat2.add(new Selectoption(n.product.Select_Category_2__c, n.product.Select_Category_2__c));         
                 opSet.add(n.product.Select_Category_2__c);
                }
            }
            cat2.sort();
            return null;
    }
    
    public PageReference doGetCat3() {  
        npsSelected = new List<ProductsWithSelection>();
        Set<String> opSet = new Set<String>();
        cat3 = new List<selectOption>();
        cat3.add(new Selectoption('', '-None-'));
        cat4 = new List<selectOption>();
        cat4.add(new Selectoption('', '-None-'));
        system.debug('selected1 ************'+selected1);
        for(ProductsWithSelection n : allProducts)
            {
             if(selected1 == n.product.Select_Category_1__c && selected2 == n.product.Select_Category_2__c)
             npsSelected.add(n);
             if(selected1 == n.product.Select_Category_1__c && selected2 == n.product.Select_Category_2__c && n.product.Select_Category_3__c != null && !opSet.contains(n.product.Select_Category_3__c))
             {
                 cat3.add(new Selectoption(n.product.Select_Category_3__c, n.product.Select_Category_3__c));         
                 opSet.add(n.product.Select_Category_3__c);
                 }
            }
            cat3.sort();
            return null;
    }

    public PageReference doGetCat4() {  
        npsSelected = new List<ProductsWithSelection>();
        Set<String> opSet = new Set<String>();
        cat4 = new List<selectOption>();
        cat4.add(new Selectoption('', '-None-'));
        system.debug('selected1 ************'+selected1);
        for(ProductsWithSelection n : allProducts)
            {
             if(selected1 == n.product.Select_Category_1__c && selected2 == n.product.Select_Category_2__c && selected3 == n.product.Select_Category_3__c)
             npsSelected.add(n);
             if(selected1 == n.product.Select_Category_1__c && selected2 == n.product.Select_Category_2__c && selected3 == n.product.Select_Category_3__c && n.product.Select_Category_4__c != null && !opSet.contains(n.product.Select_Category_4__c))
             {
                 cat4.add(new Selectoption(n.product.Select_Category_4__c, n.product.Select_Category_4__c));         
                 opSet.add(n.product.Select_Category_4__c);
                 }
            }
            cat4.sort();
            return null;
    }

    public PageReference doSearch() 
    {
        npsSelected = new List<ProductsWithSelection>();
        for(ProductsWithSelection n : allProducts)
            {
             if(selected1 == n.product.Select_Category_1__c && selected2 == n.product.Select_Category_2__c && selected3 == n.product.Select_Category_3__c && selected4 == n.product.Select_Category_4__c)
             {
                 npsSelected .add(n);
                 }
            }
     
        return null;
    }

    public class ProductsWithSelection
    {
    public Boolean selected {get; set;}
    public Network_Product_Search__c product {get; set;}    
    }
    
    public class olWraps
    {
    public String productName{get; set;}
    public opportunitylineitem oli {get; set;}    
    }

}