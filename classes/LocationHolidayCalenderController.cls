public class LocationHolidayCalenderController{
    public String rXml{get;set;}
    public String xmlString;
    public date myDate2{get;set;}
    public date newDate{get;set;}
    public String ServiceDate{get;set;}
    public String locationid{get;set;}
    public String eventString{get;set;}
    public String eventIdString{get;set;}
    public String rowId{get;set;}
    public String columnId{get;set;}
    public Boolean isEvent{get;set;}
    public Map<Integer, List<String>> indexDatesMap;
    
    public Integer total1{get;set;}
    public Integer total2{get;set;}
    public Integer total3{get;set;}
    public Integer total4{get;set;}
    public Integer total5{get;set;}
    public Integer total6{get;set;}
    public Integer total7{get;set;}
    public Integer grandTotal{get;set;}
    // public List<Location__c> Location
    public LocationHolidayCalenderController(){
        locationid = ApexPages.currentPage().getParameters().get('locationId');
        CreateXml();
    }
    
    public void CreateXml(){
        indexDatesMap = new Map<Integer, List<String>>();
        total1 = total2 = total3 = total4 = total5 = total6 = total7 = grandTotal = 0;
        locationid = ApexPages.currentPage().getParameters().get('locationId');
        System.Debug('locationid====>>>> '+ locationid);
        
        List<Event> evenList = new List<Event>();
        for(Event evnt : [Select Location, StartDateTime, EndDateTime, AccountId, OwnerId, RecurrenceStartDateTime, RecurrenceEndDateOnly From Event WHERE Subject = 'HOLIDAY' And WhatId =:locationid]){
            evenList.add(evnt);
        }
        
        Map<id,String> mapdate = new Map<id,String>();
        eventString ='';
        Time myTime = Time.newInstance(12, 0, 0, 0);
        if(evenList!=null && evenList.size()>0){
            for(Event ev:evenList){
                Time eventTime = ev.StartDateTime.time();
                if(eventTime >= myTime){
                    eventString += ev.StartDateTime.addDays(-1)+',';
                }else{
                    eventString += ev.StartDateTime+',';
                }
                System.Debug('eventTime---->'+eventTime);
            }
            System.Debug('eventString---->'+eventString);
        }
        System.Debug('evenList'+evenList);
                  
        //Parse the String format in date
        Date dte = System.Today();
        String mdy = String.ValueOf(dte);
        String dd = mdy.substring(8,10);
        String yy = mdy.substring(0,4);
        String mm = mdy.substring(5,7);
        System.Debug('mdy'+ mm + dd + yy);
        String Dat = mm + '/' + dd + '/' + yy;
        myDate2 = date.parse(Dat);
        
        //Conversion of String date into date time
        DateTime dt = DateTime.newInstance(myDate2, Time.newInstance(0, 0, 0, 0));
        
        //get Day
        String days = dt.format('EEE,MMM,MM,ww,W,dd,F');
        System.Debug('Days'+days);
        
        String Week = days.substring(11,13);
        System.Debug('Week'+Week);
        Integer dayget = 0;
        if(days.substring(0,3)== 'Sun'){
            dayget = 14;
        }
        if(days.substring(0,3)== 'Mon'){
            dayget = 15;
        }
        if(days.substring(0,3)== 'Tue'){
            dayget = 16;
        }
        if(days.substring(0,3)== 'Wed'){
            dayget = 17;
        }
        if(days.substring(0,3)== 'Thu'){
            dayget = 18;
        }
        if(days.substring(0,3)== 'Fri'){
            dayget = 19;
        }
        if(days.substring(0,3)== 'Sat'){
            dayget = 20;
        }

        List<DateTime> dateList = new List<dateTime>();
        
        rXml = '<rows>';
        
        Integer count = Integer.ValueOf(Week);
        System.Debug('IntegerCount'+count);
        
        DateTime td= System.now();
        System.Debug('dayget****'+dayget);
        Integer mainval;
        mainval = -dayget;
        
        Integer weekCount;
        Integer WeekCot = count-2;
        Integer countind = 0;
        for(weekCount= count-2; weekCount < (count-2)+53; weekCount++){
            //DateTime weekget = mydate2.addDays(-dayget);
            List<String> weakValues = new List<String>();
            DateTime findWeek = DateTime.newInstance(mydate2.addDays(-dayget), Time.newInstance(0, 0, 0, 0));
            String weekget = findweek.format('EEE,MMM,MM,ww,W,dd,F');
            System.Debug('^^^^^'+weekget);
            String Mon = weekget.substring(4,7);
            String wek = weekget.substring(11,13);
            
                rXml += '<row id="'+countind+'">'; 
                rXml += '<cell>'+ Mon +'</cell>';
                weakValues.add(Mon);
                rXml += '<cell>'+ wek+'</cell>';    
                weakValues.add(wek);
                Integer i=1;
                
                While(i<8){
                    newDate = mydate2.addDays(-dayget);
                    DateTime dtime = DateTime.newInstance(newDate, Time.newInstance(0, 0, 0, 0));
                    weakValues.add(String.valueOf(dtime));
                    String dys = dtime.format('EEE,MMM,dd');
                    if(eventString.contains(String.valueOf(newDate))){
                        rXml += '<cell class="red">'+dys.substring(8,10)+'</cell>';   
                        if(i==1) total1++;
                        if(i==2) total2++;
                        if(i==3) total3++;
                        if(i==4) total4++;
                        if(i==5) total5++;
                        if(i==6) total6++;
                        if(i==7) total7++;
                    }else{
                        rXml += '<cell>'+dys.substring(8,10)+'</cell>'; 
                    }
                    System.Debug('DaysTime@@@@'+newDate);
                    i++;   
                    dayget = dayget-1;
                }
            rXml += '</row>'; 
            indexDatesMap.put(countind, weakValues);
            countind = countind +1;
        }
        rXml += '</rows>';
        xmlString = rXml;
        grandTotal = total1 + total2 + total3 + total4 + total5 + total6 + total7;
    }
    
    public void ManageEvents(){
        List<String> WeakStatusList = indexDatesMap.get(Integer.valueOf(rowId));
        String selectedDate = WeakStatusList.get(Integer.valueOf(columnId));
        Date eventDate = Date.valueOf(selectedDate);
        DateTime eventDateTime = DateTime.newInstance(eventDate.year(),eventDate.month(),eventDate.day(),0,0,0);
        DateTime eventEndDateTime = DateTime.newInstance(eventDate.year(),eventDate.month(),eventDate.day(),23,59,0);
        
        System.debug('eventDate====>>>>'+ eventDate);
        System.debug('eventDateTime====>>>>'+ eventDateTime);
        System.debug('eventEndDateTime====>>>>'+ eventEndDateTime);
        if(!isEvent){
            System.debug('locationId====>>>>'+ locationId);
            Event event = new Event();
            event.Subject = 'HOLIDAY';
            event.whatId = locationId;
            event.StartDateTime = eventDateTime;
            event.EndDateTime = eventDateTime.addHours(1);
            event.Outcome__c = 'Appointment Scheduled';
            
            insert event;
        }else{
            List<Event> events = [SELECT Id, Subject, StartDateTime FROM Event 
                                  WHERE StartDateTime >=: eventDateTime 
                                  AND StartDateTime <=: eventEndDateTime
                                  AND Subject = 'HOLIDAY'];
            if(events != null && events.size() > 0){
                delete events;
            }
        }
        CreateXml();
    }
}