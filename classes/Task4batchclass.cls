/*  ClassName   :   Task4BatchClass
    CreateBy    :   Ch.V.GopiNadh
    Date        :   19/12/2013  
    Description :   batch apex class to update all contacts with a datetime field to current datetime and 
                    schedule it to run for every 1 hour. */ 


global class Task4batchclass implements database.Batchable<contact> { 
    
    public iterable<contact> start(Database.BatchableContext BC){
        contact[] c=[select id,email,DateTime__c from contact];
        return c;
    }
    
    /*
 *  Method name     :   execute 
 * Created by       :   Ch.V.Gopinadh
 * Date             :   19/12/2013
 * Description      :   Method which returns the current system date and assign it to the contact datetime field
 * Input parameters :   Database.BatchableContext BC, list<contact> scope
 * Returns          :   contact list
 */
    
    public void execute(Database.BatchableContext BC, list<contact> scope){
        list<contact> clist=new list<contact>();
        for(contact c1:scope){  
            
        c1.DateTime__c= System.now();
        
        clist.add(c1);
        }
        update clist;
    }
    public void finish(Database.BatchableContext BC){
        
        
    }

}