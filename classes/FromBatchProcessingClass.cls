global class FromBatchProcessingClass implements Database.Batchable<sObject> ,Database.AllowsCallouts,Database.Stateful 
{
  
   global Map<String,String> body = new Map<String,String>();
   global Database.QueryLocator start(Database.BatchableContext BC)
    {
       String query = 'SELECT Id,Name,Email_Template_Body__c,Template_Id__c FROM Store_Amazon_Data__c';
       return Database.getQueryLocator(query); 
    }
 
    global void execute(Database.BatchableContext BC,List<Store_Amazon_Data__c> scope)
    {    
        //System.debug('////////////////'+testvar);
        Amazon_SES_SendEmail sendemail = new Amazon_SES_SendEmail();
        for(Store_Amazon_Data__c s:scope){
        sendemail.sendEmailFromAmazon('gopinath@tvarana.com','gopinath@tvarana.com',
                                        'Approval_process_based_on_Amount',s.Email_Template_Body__c,'text');
        }
    }   
    global void finish(Database.BatchableContext BC)
    {
       
    }
}