global class SendEmailAfter5days implements Database.Batchable<Contact>
{
    global List<contact> start(Database.BatchableContext bc)
    {
          return null ;
    }
   
    global void execute(Database.BatchableContext bc,List<Contact> recs)
    {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        
            System.debug('execute method called');
            List<String> toAddresses = new List<String>();           
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            toAddresses.add('gopinath@tvarana.com');
            mail.setToAddresses(toAddresses);
            mail.setSubject('Your Contact Got Expired');
            String messageBody = '<html><body>Hi ' + 'Gopinath' + ',<br>Your account Expires today. <br>Kindly contact your administrator.<br><br><b>Regards,</b><br>Ch.V.Gopinadh</body></html>';
            mail.setHtmlBody(messageBody); 
            mailList.add(mail);          
         
        Messaging.sendEmail(mailList);
    }
   
    global void finish(Database.BatchableContext bc)
    {
    }
}