public class dataTableCon {

        public Boolean Account_Closed {get; set;}                
        List<Account> accounts{get;set;}
                
        public List<Account> getAccounts() {

                if(accounts == null) accounts = [select name,Rating,Status__c,Account_Closed__c from account limit 10];

                return accounts;

        }
        public PageReference save(){
            System.debug('111111111'+Account_Closed);
            Id aid = apexpages.currentPage().getParameters().get('aid');
            Account a = [select id , name , Account_Closed__c from Account where id = : aid ];
            if(Account_Closed == True){
            a.Account_Closed__c = True;
            }
            update a;
           
            PageReference p = new PageReference('/apex/CreatingCustomListView');
            p.setRedirect(true);
            return p;
           
        }
        public PageReference remove(){
            Id aid = apexpages.currentPage().getParameters().get('did');
            Account a = [select id , Account_closed__c from Account where id = : aid ];
            delete(a);
            PageReference p = new PageReference('/apex/CreatingCustomListView');
            p.setRedirect(true);
            return p;
           
        }

}