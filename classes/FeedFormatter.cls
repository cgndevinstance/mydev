Public class FeedFormatter {
    Public ConnectApi.FeedItemPage feedItemPage{get;set;}
    Public FeedItem item{get;set;}
    Public FeedFormatter (){
        item = new FeedItem();
        feedItemPage = ConnectApi.ChatterFeeds.getFeedItemsFromFeed(null,ConnectApi.FeedType.News,'me');
    }
}