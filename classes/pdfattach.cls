public class pdfattach {
    Blob body;
    public pdfattach(ApexPages.StandardController controller) {
       
    }
      public PageReference attachment(){
        PageReference pdf = page.Generatepdf;
        Attachment attach = new Attachment();
        try {
 
        // returns the output of the page as a PDF
        body = pdf.getContentAsPDF();
 
    // need to pass unit test -- current bug    
    } catch (VisualforceException e) {
        body = Blob.valueOf('Some Text');
    }
     attach.Body = body;
    // add the user entered name
    attach.Name = 'sample.pdf';
    attach.IsPrivate = false;
    // attach the pdf to the account
    attach.ParentId = ApexPages.CurrentPage().getParameters().get('Id');
    insert attach;
    PageReference p = new PageReference('https://ap1.salesforce.com/'+ApexPages.CurrentPage().getParameters().get('Id'));
    p.setRedirect(true);
    return null;
    }

}