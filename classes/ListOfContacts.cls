public class ListOfContacts {
    @auraEnabled
    public static List<Contact> conList() {
        List<Contact> contactsList = [select id,name,Email from contact limit 25];
        return contactsList;
    }
}