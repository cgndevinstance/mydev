Public class AccountTreeStrucutureController {
   
    // ******* Variables,Map,List  Declaration *********
    
    List<Account> ACList{get;set;}
    Map<ID, List<ID>> mapParentToChildren = null;
    List<Account> AllAccounts {get;set;}
    String AllRecords = '';
    Public string totalString{get;set;}
    Public String TotalString1{get;set;}
    Public String accountsJSON{get;set;}
    Map<Id,String> ACIdNames = null;
   
    
    // ******* Constructor Declaration ********
    
    Public AccountTreeStrucutureController(){
    
        ACIdNames = new Map<Id,String>();
        totalString = '';
        AllAccounts = new List<Account>();
        mapParentToChildren = new Map<ID, list<id>>();
        AllAccounts = [ SELECT Id,ParentId,Name from Account ];
        // ******* Adding All Account ids and thier Names to Map *******
        for( Account s1 : AllAccounts )
        {
            ACIdNames.put(s1.id,s1.name);
        }
        
        // ******* Adding All Account ParentIds and thier Ids to Another Map *******
        for( Account[] sRecordArr : [ SELECT Id,ParentId,Name from Account ] )
        {
            for( Account sRecord : sRecordArr )
            {
                if( mapParentToChildren.get(sRecord.ParentId) == NULL )
                {
                    mapParentToChildren.put( sRecord.ParentId, new List<ID>{ sRecord.Id } );
                }
                else
                {
                    mapParentToChildren.get( sRecord.ParentId ).add( sRecord.Id );
                }
            }
        }
       
        // Top level Records will have a null parent, so be in the 'NULL' list
        for(id idRecord : mapParentToChildren.get(NULL))
        {
            PrintTree(idRecord, 0,2);
        }
        TotalString1 = totalString;
    }
    
    Public void PrintTree(ID idRecord, Integer iLevel,Integer imgNum)
    {
        String strLevel = '*';
        If(iLevel == 0)
            AllRecords = '';
        AllRecords += idRecord;
        
        if( imgNUM > 7 )
            imgNum = 2;
        totalString += '<li><span><img src="/resource/1399541679000/Images/Images/'+imgNum+'.gif" style="margin-top:-5px;height:18px" />'+ ACIdNames.get(idRecord) +'</span>';
        
        
        for(integer i = 0; i < iLevel; i++)
        {
            strLevel += '*';
        }
        
        if(mapParentToChildren.get(idRecord) != null)
        {
            totalString += '<ul>';
            for(id idChild : mapParentToChildren.get(idRecord))
            {
                
                PrintTree(idChild, iLevel + 1,imgNum+1);
            }
            totalString += '</ul>';
        }
        totalString += '</li>';
    }
}