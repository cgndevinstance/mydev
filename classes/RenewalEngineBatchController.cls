public with sharing class RenewalEngineBatchController implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
    Date FromDate = null;
    Date ToDate = null;
    Decimal UpliftPercentage = 0;
    public string SOLineItmesFields = '';
    public Integer SalesOrderProcessed = 0;
    public Integer OppCreated = 0;
    Public Integer OppFailed = 0;
    public RenewalEngineBatchController(Date FromDate,Date ToDate,Decimal UpliftPercentage) {
        this.FromDate = FromDate;
        this.ToDate = ToDate;
        this.UpliftPercentage = UpliftPercentage;
    }
    public Database.QueryLocator start(Database.BatchableContext BC) {
        
            
            string SalesOrderLineItemsQuery = 'select Account__c,Considered_for_Renewal__c,Subscription_End_Date__c from Captain__c where Subscription_End_Date__c>=:FromDate and '+
                                       'Subscription_End_Date__c<=:ToDate and '+
                                       'Considered_for_Renewal__c = null and Account__r.Do_Not_Renew__c = false';
             
            string AccountQuery = 'select id from Account where  (( MinDate__c>=:FromDate and '+
                                       'MinDate__c<=:ToDate ) Or '+
                                       '( MaxDate__c>=:FromDate and MaxDate__c<=:ToDate ))'+
                                       +' and Do_Not_Renew__c = false';
           system.debug('AccountQueryAccountQuery'+AccountQuery);           
        return Database.getQueryLocator(AccountQuery);
    }
    public void execute (Database.BatchableContext BC, List<SObject> scope) {
        
           /* Set<Id> SaleOrderLineIdsset = new Set<Id>();
            List<Opportunity> InsertOpportunityList = new List<Opportunity>();
            List<OpportunityLineItem> OpportunityLineList = new List<OpportunityLineItem>();
            List<Captain__c> UpdateSOLineItems = new List<Captain__c>();
            Map<Id,List<Captain__c>> MapSalesOrdersWithItems = new Map<Id,List<Captain__c>>();
            for ( SObject Sitem : scope) {
                 Captain__c LineItem = (Captain__c)Sitem;
                    if ( LineItem.Account__c != null ) {
                        SaleOrderLineIdsset.add(LineItem.Account__c);
                    }
                
            }
            Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Account').getDescribe().fields.getMap();
            String qString = '';
            for(Schema.SObjectField sfield : fieldMap.Values()) {
                 schema.describefieldresult dfield = sfield.getDescribe();
                 if(qString=='')
                     qString+= string.valueOf(dfield.getname());
                 else
                     qString+= ','+string.valueOf(dfield.getname());
            }
            String SalesOrderQuery = 'select '+qString+' from Account where id in:SaleOrderLineIdsset and Do_Not_Renew__c = false';
            System.debug('SalesOrderQuerySalesOrderQuery'+SalesOrderQuery);
            List<Account> SalesorderList = Database.query(SalesOrderQuery);
            string SOLineItemsQuery = 'select Account__c,Considered_for_Renewal__c,Subscription_End_Date__c from Captain__c where Account__c in:SaleOrderLineIdsset and Considered_for_Renewal__c = null';
            System.debug('SOLineItemsQuerySOLineItemsQuery'+SOLineItemsQuery);
            List<Captain__c> SalesOrderLineItemsList = Database.query(SOLineItemsQuery);
            for (Captain__c SOItem : SalesOrderLineItemsList ) {
                
                    if ( MapSalesOrdersWithItems.containsKey(SOItem.Account__c) ) {
                         MapSalesOrdersWithItems.get(SOItem.Account__c).add(SOItem);
                    } else {
                         MapSalesOrdersWithItems.put(SOItem.Account__c,new List<Captain__c>{SOItem});   
                    }
               
            }
            for (Account SaleSOrder : SalesorderList) {
                   Opportunity opp = new Opportunity();
                   opp.Name = 'Renewal Opportunity-'+SaleSOrder.Name;
                   opp.StageName = 'Renewal Upgraded';
                   opp.CloseDate = System.today();
                   
                   InsertOpportunityList.add(opp);
               
               
           }
           set<Id> InsertedOppIds = new Set<Id>();
           Map<Id,Id> MapSalesAndOpp = new Map<Id,Id>();
           if ( InsertOpportunityList.size() > 0 ) {
               
               Database.SaveResult[] Result = Database.insert(InsertOpportunityList,false);
               System.debug('111111111111111111'+Result);
               for ( Database.SaveResult rest : Result ) {
                   if ( rest.isSuccess() == true) {
                       InsertedOppIds.add(rest.getId());
                       OppCreated += 1;
                   } else {
                       OppFailed += 1;
                   }
               }
           }
           if (InsertedOppIds.size() > 0 ) {
               List<Opportunity> OppList = [select AccountId from Opportunity where id in:InsertedOppIds];
               for ( Opportunity opp : OppList ) {
                   MapSalesAndOpp.put(opp.AccountId,opp.id);
               }
           }
           
          
           SalesOrderProcessed += SalesorderList.size();
      */
      
    }
    public void finish (Database.BatchableContext BC) {
       try {  
            List<AsyncApexJob> job = [SELECT Id,status,JobItemsProcessed,NumberOfErrors,TotalJobItems
                                    FROM AsyncApexJob
                                    WHERE id =: BC.getJobId()];
           if ( job.size() > 0 ) {
               string MsgBody = '';
               MsgBody += 'Number Of Jobs : '+job[0].TotalJobItems+'\n'  ;
               MsgBody += 'Number Of Jobs Processed : '+job[0].JobItemsProcessed+'\n'  ;
               MsgBody += 'Number Of Errors : '+job[0].NumberOfErrors+'\n'  ;
               MsgBody += 'No of Sale Orders Processed : '+SalesOrderProcessed+'\n'  ;
               MsgBody += 'No of Renewal Opportunites Created : '+OppCreated+'\n'  ;
               MsgBody += 'No of Opportunity Renewal Failures : '+OppFailed+'\n'  ;
               Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
               string email = UserInfo.getUserEmail();
               String[] toAddresses = new String[] {'gopinath@tvarana.com'};
               message.setPlainTextBody(MsgBody);
               message.saveAsActivity=false;
               message.setToAddresses(toAddresses);
               Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message });
           }
       }catch ( Exception e) {
       
       }
    }
}